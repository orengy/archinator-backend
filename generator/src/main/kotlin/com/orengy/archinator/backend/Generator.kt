package com.orengy.archinator.backend

import com.google.auto.service.AutoService
import com.google.common.base.CaseFormat
import com.google.common.reflect.TypeToken
import com.squareup.kotlinpoet.*
import java.io.File
import javax.annotation.processing.AbstractProcessor
import javax.annotation.processing.ProcessingEnvironment
import javax.annotation.processing.Processor
import javax.annotation.processing.RoundEnvironment
import javax.lang.model.SourceVersion
import javax.lang.model.element.Element
import javax.lang.model.element.ElementKind
import javax.lang.model.element.TypeElement
import javax.lang.model.type.ExecutableType
import javax.lang.model.type.TypeMirror
import javax.tools.Diagnostic


@AutoService(Processor::class)
class Generator : AbstractProcessor() {

    @Synchronized
    override fun init(processingEnv: ProcessingEnvironment) {
        super.init(processingEnv)
    }

    override fun getSupportedAnnotationTypes(): MutableSet<String> {
        println("getSupportedAnnotationTypes")
        return mutableSetOf(
                SyncDown::class.java.name,
                SyncFile::class.java.name,
                Sync::class.java.name
        )
    }

    override fun getSupportedSourceVersion(): SourceVersion {
        return SourceVersion.latest()
    }

    override fun process(set: MutableSet<out TypeElement>, roundEnv: RoundEnvironment): Boolean {

        val syncDownClasses = mutableListOf<String>()
        val syncClasses = mutableListOf<String>()
        var pack = ""

        val records = mutableListOf<Element>()

        val syncDown = roundEnv.getElementsAnnotatedWith(SyncDown::class.java)
        for (it in syncDown) {
            System.err.println("Processing: " + it.simpleName.toString())
            println("Processing: " + it.simpleName.toString())
            generateSyncDown(it)
            syncDownClasses.add(it.simpleName.toString())
            pack = processingEnv.elementUtils.getPackageOf(it).toString()
            records.add(it)
        }

        val sync = roundEnv.getElementsAnnotatedWith(Sync::class.java)
        for (it in sync) {
            println("Processing: " + it.simpleName.toString())
            generateSync(it)
            syncClasses.add(it.simpleName.toString())
            pack = processingEnv.elementUtils.getPackageOf(it).toString()
            records.add(it)
        }

        val syncFile = roundEnv.getElementsAnnotatedWith(SyncFile::class.java)
        for (it in syncFile) {
            println("Processing: " + it.simpleName.toString())
            generateSyncFile(it)
            syncClasses.add(it.simpleName.toString())
            pack = processingEnv.elementUtils.getPackageOf(it).toString()
            records.add(it)
        }

        if (syncDownClasses.size + syncClasses.size == 0) {
            return true
        }


        val unmarkedNodes = mutableListOf<String>()
        val nodeMark = hashMapOf<String, NodeMark>()
        val edges = hashMapOf<String, MutableList<String>>()
        val orderedRecords = mutableListOf<String>()
        for (it in records) {
            nodeMark[it.simpleName.toString()] = NodeMark.UNMARKED
            unmarkedNodes.add(it.simpleName.toString())
            edges[it.simpleName.toString()] = mutableListOf()
        }
        for (r in records) {
            r.enclosedElements.filter {
                it.kind == ElementKind.METHOD &&
                        it.simpleName.startsWith("get") &&
                        nodeMark.containsKey((it.asType() as ExecutableType).returnType.asTypeName().toString().substringAfterLast("."))

            }.forEach {
                        val t = (it.asType() as ExecutableType).returnType.asTypeName().toString().substringAfterLast(".")
                        if (!edges[r.simpleName.toString()]!!.contains(t))
                            edges[r.simpleName.toString()]!!.add(t)
                    }
        }


//        edges.entries.forEach {
//            processingEnv.messager.printMessage(Diagnostic.Kind.NOTE, it.key + " -> " + it.value.joinToString())
//        }

        while (!unmarkedNodes.isEmpty()) {
            visit(unmarkedNodes.first(), unmarkedNodes, nodeMark, edges, orderedRecords)
        }

//        processingEnv.messager.printMessage(Diagnostic.Kind.NOTE, orderedRecords.joinToString(separator = " -> "))


        generateConstants(orderedRecords, pack)
        generateDeserializer(syncClasses, syncDownClasses, pack)

        return true
    }

    internal fun visit(
            node: String,
            unmarkedNodes: MutableList<String>,
            nodeMark: HashMap<String, NodeMark>,
            edges: HashMap<String, MutableList<String>>,
            orderedNodes: MutableList<String>) {
        if (nodeMark[node] == NodeMark.MARKED)
            return
        if (nodeMark[node] == NodeMark.TEMPORARY_MARKED) {
            throw Exception("Cycle in records")
        }

        nodeMark[node] = NodeMark.TEMPORARY_MARKED
        for (m in edges[node]!!) {
            visit(m, unmarkedNodes, nodeMark, edges, orderedNodes)
        }
        nodeMark[node] = NodeMark.MARKED
        unmarkedNodes.remove(node)
        orderedNodes.add(node)
    }

    internal enum class NodeMark {
        UNMARKED,
        TEMPORARY_MARKED,
        MARKED
    }

    private fun generateConstants(
            records: List<String>,

            pack: String
    ) {

        val syncObject = TypeSpec.objectBuilder("Sync")
                .addProperty(PropertySpec.builder("ACTION_DELETE", String::class)
                        .mutable(false)
                        .initializer(""""delete"""")
                        .build())
                .addProperty(PropertySpec.builder("ACTION_UPDATE", String::class)
                        .mutable(false)
                        .initializer(""""update"""")
                        .build())
                .addProperty(PropertySpec.builder("ACTION_INSERT", String::class)
                        .mutable(false)
                        .initializer(""""insert"""")
                        .build())

        val mappings = mutableListOf<String>()

        for (s in records) {
            syncObject.addProperty(PropertySpec.builder(
                    CaseFormat.UPPER_CAMEL.to(CaseFormat.UPPER_UNDERSCORE, s), String::class)
                    .mutable(false)
                    .initializer("\"" + CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, s) + "\"")
                    .build())
            mappings.add(CaseFormat.UPPER_CAMEL.to(CaseFormat.UPPER_UNDERSCORE, s) + " to " + s + classNameAddition + "()")
        }

        syncObject.addProperty(
                PropertySpec.builder("syncTypes",
                        ParameterizedTypeName.get(
                                ClassName("kotlin.collections", "Map"),
                                String::class.asTypeName(),
                                ParameterizedTypeName.get(
                                        ClassName("com.orengy.archinator.backend.model.sync.orm.base", "BaseRecord"),
                                        ClassName(pack, "*")
                                )
                        ))

                        .mutable(false)
                        .initializer("mapOf(" + mappings.joinToString(separator = ", ") + ")")
                        .build())


        val file = FileSpec.builder(pack, "Sync")
                .addType(syncObject.build())
                .build()

        val kaptKotlinGeneratedDir = processingEnv.options[KAPT_KOTLIN_GENERATED_OPTION_NAME]
        file.writeTo(File(kaptKotlinGeneratedDir, "Sync.kt"))
    }

    private fun generateDeserializer(
            syncClasses: List<String>,
            syncDownClasses: List<String>,
            pack: String
    ) {
        val deserializer = TypeSpec.classBuilder("SyncModelDeserializer")
                .addSuperinterface(ParameterizedTypeName.get(
                        ClassName("com.google.gson", "JsonDeserializer"),
                        ClassName("com.orengy.archinator.backend.model.sync", "SyncBaseModel")
                ))
                .addFunction(FunSpec.builder("deserialize")
                        .addModifiers(KModifier.OVERRIDE)
                        .addParameter(ParameterSpec.builder("json",
                                ClassName("com.google.gson", "JsonElement").asNullable()).build())
                        .addParameter(ParameterSpec.builder("typeOfT",
                                ClassName("java.lang.reflect", "Type").asNullable()).build())
                        .addParameter(ParameterSpec.builder("context",
                                ClassName("com.google.gson", "JsonDeserializationContext").asNullable()).build())
                        .returns(ClassName("com.orengy.archinator.backend.model.sync", "SyncBaseModel").asNullable())
                        .addCode("" +
                                "val type = json?.asJsonObject?.get(\"type\")?.asString\n" +
                                "val hasData = json?.asJsonObject?.has(\"data\") ?: false\n" +
                                "return when (type) {\n" +
                                syncDownClasses.joinToString(separator = ", ") {
                                    "Sync." + CaseFormat.UPPER_CAMEL.to(CaseFormat.UPPER_UNDERSCORE, it)
                                } +
                                "         ->\n" +
                                "        context?.deserialize(json.asJsonObject, SyncDownStatusModel::class.java)\n" +
                                "    null -> null\n" +
                                "    else -> {\n" +
                                "        return if (hasData) {\n" +
                                "            when (type) {" +
                                syncClasses.joinToString(separator = "\n") {
                                    "Sync." + CaseFormat.UPPER_CAMEL.to(CaseFormat.UPPER_UNDERSCORE, it) + "-> {\n" +
                                            "                    val t = object : TypeToken<SyncFullModel<${it + classNameAddition}>>() {}.type\n" +
                                            "                    return context?.deserialize(json.asJsonObject, t)\n" +
                                            "                }\n"

                                } +

                                "                else -> null\n" +
                                "            }\n" +
                                "        } else {\n" +
                                "            context?.deserialize(json.asJsonObject, SyncStatusModel::class.java)\n" +
                                "        }\n" +
                                "    }\n" +
                                "}"
                        )
                        .build()
                )


        val file = FileSpec.builder(pack, "SyncModelDeserializer")
                .addType(deserializer.build())
                .addStaticImport("com.google.gson.reflect", "TypeToken")
                .addStaticImport("com.orengy.archinator.backend.model.sync", "SyncFullModel", "SyncStatusModel", "SyncDownStatusModel")
                .build()

        val kaptKotlinGeneratedDir = processingEnv.options[KAPT_KOTLIN_GENERATED_OPTION_NAME]
        file.writeTo(File(kaptKotlinGeneratedDir, "SyncModelDeserializer.kt"))
    }

    private fun generateSyncDown(element: Element) {

        val className = element.simpleName.toString()
        val classNameUpperUnderscore = CaseFormat.UPPER_CAMEL.to(CaseFormat.UPPER_UNDERSCORE, className)
        val fileName = className + classNameAddition
        val pack = processingEnv.elementUtils.getPackageOf(element).toString()


        val fetchDataCode = CodeBlock.builder().add(
                "" +
                        "return Db.main<Result<Record>> {" +
                        "return@main it" +
                        ".select(listOf(")

        val fieldList = mutableListOf("$classNameUpperUnderscore.ID")


        val fields = element.enclosedElements.filter { it.simpleName.startsWith("get") }

        for (f in fields) {
            val t = CaseFormat.UPPER_CAMEL.to(CaseFormat.UPPER_UNDERSCORE, f.simpleName.substring(3)) + if ((f.asType() as ExecutableType).returnType.asTypeName().asNonNullable().toString().startsWith(pack)) "_ID" else ""

            fieldList.add("$classNameUpperUnderscore." + t +
                    if (t == classNameUpperUnderscore) "_" else ""

            )
        }
        fetchDataCode.add(fieldList.joinToString(separator = ", "))

        fetchDataCode.add("))" +
                ".from($classNameUpperUnderscore)" +
                ".fetch()" +
                "}")

        val recordClass = TypeSpec.classBuilder(fileName)
                .superclass(
                        ParameterizedTypeName.get(
                                ClassName("com.orengy.archinator.backend.model.sync.orm.base", "SyncDownRecord"),
                                ClassName("com.orengy.archinator.backend.model.sync.orm", fileName)
                        )
                )
                .addFunction(FunSpec.builder("fetchDbData")
                        .addModifiers(KModifier.OVERRIDE)
                        .addParameter(
                                "user",
                                ClassName("com.orengy.archinator.backend.model", "User")

                        )
                        .returns(ParameterizedTypeName.get(
                                ClassName("org.jooq", "Result"),
                                ClassName("org.jooq", "Record")
                        ))
                        .addCode(
                                fetchDataCode.build()
                        )
                        .build())
                .addProperty(PropertySpec.builder(
                        "listType",
                        ClassName("java.lang.reflect", "Type"),
                        KModifier.OVERRIDE)
                        .addAnnotation(Transient::class)
                        .mutable(false)
                        .initializer("object : TypeToken<List<$fileName>>() {}.type")
                        .build())

        for (f in fields) {

            val t = (f.asType() as ExecutableType).returnType.asTypeName().asNonNullable().toString()
            val typeName =
                    if (t.startsWith(pack)) {
                        ClassName(pack, t.substringAfterLast(".") + classNameAddition).asNullable()
                    } else {
                        when (t) {
                            "java.lang.Integer" -> ClassName("kotlin", "Int").asNullable()
                            "java.lang.String" -> ClassName("kotlin", "String").asNullable()
                            "java.lang.Double" -> ClassName("kotlin", "Double").asNullable()
                            else -> (f.asType() as ExecutableType).returnType.asTypeName().asNullable()
                        }
                    }

            recordClass.addProperty(PropertySpec.builder(
                    CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_CAMEL, f.simpleName.substring(3)),
                    typeName)
                    .mutable(true)
                    .initializer("null")
                    .build())
        }


        val file = FileSpec.builder(pack, fileName)
                .addStaticImport("com.orengy.archinator.backend.utils", "Db")
                .addStaticImport("com.orengy.archinator.db", "Tables.*")
                .addStaticImport("org.modelmapper", "TypeToken")
                .addType(recordClass.build())
                .build()

        val kaptKotlinGeneratedDir = processingEnv.options[KAPT_KOTLIN_GENERATED_OPTION_NAME]
        file.writeTo(File(kaptKotlinGeneratedDir, "$fileName.kt"))
    }

    private fun generateSync(element: Element) {

        val className = element.simpleName.toString()
        val classNameUpperUnderscore = CaseFormat.UPPER_CAMEL.to(CaseFormat.UPPER_UNDERSCORE, className)

        val fileName = className + classNameAddition
        val pack = processingEnv.elementUtils.getPackageOf(element).toString()


        val fieldList = mutableListOf<String>(
                "$classNameUpperUnderscore.CREATED_AT"
        )
        val fieldListData = mutableListOf<String>(
                "data.createdAt!!"
        )
        val fieldListAndData = mutableListOf<Pair<String, String>>(
                Pair("$classNameUpperUnderscore.CREATED_AT", "data.createdAt!!")
        )

        val fields = element.enclosedElements.filter { it.simpleName.startsWith("get") }

        for (f in fields) {
            val b = (f.asType() as ExecutableType).returnType.asTypeName().asNonNullable().toString().startsWith(pack)

            val t = CaseFormat.UPPER_CAMEL.to(CaseFormat.UPPER_UNDERSCORE, f.simpleName.substring(3)) +
                    if (b) "_ID" else ""

            fieldList.add("$classNameUpperUnderscore." + t +
                    if (t == classNameUpperUnderscore) "_" else ""
            )

            val t2 = CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_CAMEL, f.simpleName.substring(3)) +
                    (if (b) "?.id" else "")

            val data =
                    if (b)
                        "insertedItemsIds[Pair(\"${CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, f.simpleName.substring(3))}\", data.$t2!!)] ?: data.$t2!!"
                    else
                        "data.$t2!!"
            fieldListData.add(data)
//            fieldListData.add("data.$t2!!")

            fieldListAndData.add(Pair("$classNameUpperUnderscore." + t +
                    if (t == classNameUpperUnderscore) "_" else "", data))
        }

        val fetchDataCode = CodeBlock.builder().add(
                "" +
                        "return Db.main<Result<Record>> {\n" +
                        "return@main it\n" +
                        ".select(listOf(\n")

        fetchDataCode.add("$classNameUpperUnderscore.ID,\n" + fieldList.joinToString(separator = ",\n"))

        fetchDataCode.add("))" +
                ".from($classNameUpperUnderscore)\n" +
                ".where($classNameUpperUnderscore.USER_ID.eq(user.userId))\n" +
                ".fetch()\n" +
                "}\n")

        val insertItemToDbCode = CodeBlock.builder().add("" +
                "return Db.main<$fileName> {\n" +
                "val res = it.insertInto($classNameUpperUnderscore,\n" +
                "$classNameUpperUnderscore.USER_ID,\n " +
                fieldList.joinToString(separator = ",\n") + ")" +
                ".values(\n" +
                "user.userId,\n" +
                fieldListData.joinToString(separator = ",\n") + ")" +
                ".returning(\n" +
                "$classNameUpperUnderscore.ID,\n" +
                fieldList.joinToString(separator = ",\n") + ")" +
                ".fetchOne();\n" +
                "return@main SyncManager.modelMapper.map(res, $fileName::class.java)\n" +
                "}\n"
        )

        val isDbRecordNewerCode = CodeBlock.builder().add("" +
                "return Db.main<Boolean> \n{" +
                "val res = it.select($classNameUpperUnderscore.UPDATED_AT)\n" +
                ".from($classNameUpperUnderscore)\n" +
                ".where($classNameUpperUnderscore.ID.eq(id))\n" +
                ".first(); \n" +
                "val timeStamp = res [$classNameUpperUnderscore.UPDATED_AT]; \n" +
                "return@main timeStamp.time > updatedAt \n}")

        val updateDbRecordCode = CodeBlock.builder().add("" +
                "Db.main {\n" +
                "it.update($classNameUpperUnderscore)\n" +
                fieldListAndData.joinToString(separator = "\n") {
                    ".set(${it.first}, ${it.second})"
                } +
                ".where($classNameUpperUnderscore.ID.eq(data.id))\n" +
                ".execute()\n" +
                "}\n")


        val recordClass = TypeSpec.classBuilder(fileName)
                .superclass(
                        ParameterizedTypeName.get(
                                ClassName("com.orengy.archinator.backend.model.sync.orm.base", "SyncRecord"),
                                ClassName("com.orengy.archinator.backend.model.sync.orm", fileName)
                        )
                )
                .addFunction(FunSpec.builder("fetchDbData")
                        .addModifiers(KModifier.OVERRIDE)
                        .addParameter(
                                "user",
                                ClassName("com.orengy.archinator.backend.model", "User")

                        )
                        .returns(ParameterizedTypeName.get(
                                ClassName("org.jooq", "Result"),
                                ClassName("org.jooq", "Record")
                        ))
                        .addCode(
                                fetchDataCode.build()
                        )
                        .build())
                .addFunction(FunSpec.builder("insertItemToDb")
                        .addModifiers(KModifier.OVERRIDE)
                        .addParameter(
                                "data",
                                ClassName(pack, fileName)
                        )
                        .addParameter(
                                "user",
                                ClassName("com.orengy.archinator.backend.model", "User")
                        )
                        .addParameter(
                                "insertedItemsIds",
                                ParameterizedTypeName.get(
                                        ClassName("kotlin.collections", "HashMap"),
                                        ParameterizedTypeName.get(
                                                Pair::class,
                                                String::class,
                                                Int::class
                                        ),
                                        Int::class.asTypeName()
                                )

                        )
                        .returns(
                                ClassName(pack, fileName)
                        )
                        .addCode(
                                insertItemToDbCode.build()
                        )
                        .build())
                .addFunction(FunSpec.builder("updateDbRecord")
                        .addModifiers(KModifier.OVERRIDE)
                        .addParameter(
                                "data",
                                ClassName(pack, fileName)
                        )
                        .addParameter(
                                "insertedItemsIds",
                                ParameterizedTypeName.get(
                                        ClassName("kotlin.collections", "HashMap"),
                                        ParameterizedTypeName.get(
                                                Pair::class,
                                                String::class,
                                                Int::class
                                        ),
                                        Int::class.asTypeName()
                                )

                        )
                        .addCode(
                                updateDbRecordCode.build()
                        )
                        .build())
                .addFunction(FunSpec.builder("isDbRecordNewer")
                        .addModifiers(KModifier.OVERRIDE)
                        .addParameter(
                                "updatedAt",
                                ClassName("kotlin", "Long")
                        )
                        .addParameter(
                                "id",
                                ClassName("kotlin", "Int")
                        )
                        .returns(
                                ClassName("kotlin", "Boolean")
                        )
                        .addCode(
                                isDbRecordNewerCode.build()
                        )
                        .build())
                .addProperty(PropertySpec.builder(
                        "listType",
                        ClassName("java.lang.reflect", "Type"),
                        KModifier.OVERRIDE)
                        .addAnnotation(Transient::class)
                        .mutable(false)
                        .initializer("object : TypeToken<List<$fileName>>() {}.type")
                        .build())

        for (f in fields) {

            val t = (f.asType() as ExecutableType).returnType.asTypeName().asNonNullable().toString()
            val typeName =
                    if (t.startsWith(pack)) {
                        ClassName(pack, t.substringAfterLast(".") + classNameAddition).asNullable()
                    } else {
                        when (t) {
                            "java.lang.Integer" -> ClassName("kotlin", "Int").asNullable()
                            "java.lang.String" -> ClassName("kotlin", "String").asNullable()
                            "java.lang.Double" -> ClassName("kotlin", "Double").asNullable()
                            else -> (f.asType() as ExecutableType).returnType.asTypeName().asNullable()
                        }


                    }

            recordClass.addProperty(PropertySpec.builder(
                    CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_CAMEL, f.simpleName.substring(3)),
                    typeName)
                    .mutable(true)
                    .initializer("null")
                    .build())
        }


        val file = FileSpec.builder(pack, fileName)
                .addStaticImport("com.orengy.archinator.backend.utils", "Db")
                .addStaticImport("com.orengy.archinator.db", "Tables.*")
                .addStaticImport("org.modelmapper", "TypeToken")
                .addStaticImport("com.orengy.archinator.backend.model.sync", "SyncManager")
                .addType(recordClass.build())
                .build()

        val kaptKotlinGeneratedDir = processingEnv.options[KAPT_KOTLIN_GENERATED_OPTION_NAME]
        file.writeTo(File(kaptKotlinGeneratedDir, "$fileName.kt"))
    }

    private fun generateSyncFile(element: Element) {

        val className = element.simpleName.toString()
        val classNameUpperUnderscore = CaseFormat.UPPER_CAMEL.to(CaseFormat.UPPER_UNDERSCORE, className)
        val fileName = className + classNameAddition
        val pack = processingEnv.elementUtils.getPackageOf(element).toString()


        val fieldList = mutableListOf<String>(
                "$classNameUpperUnderscore.REMOTE_FILE_URL",
                "$classNameUpperUnderscore.CREATED_AT"
        )
        val fieldListData = mutableListOf<String>(
                "data.remoteFileUrl!!",
                "data.createdAt!!"
        )
        val fieldListAndData = mutableListOf<Pair<String, String>>(
                Pair("$classNameUpperUnderscore.REMOTE_FILE_URL", "data.remoteFileUrl!!"),
                Pair("$classNameUpperUnderscore.CREATED_AT", "data.createdAt!!")
        )

        val fields = element.enclosedElements.filter { it.simpleName.startsWith("get") }

        for (f in fields) {
            val b = (f.asType() as ExecutableType).returnType.asTypeName().asNonNullable().toString().startsWith(pack)

            val t = CaseFormat.UPPER_CAMEL.to(CaseFormat.UPPER_UNDERSCORE, f.simpleName.substring(3)) +
                    if (b) "_ID" else ""

            fieldList.add("$classNameUpperUnderscore." + t +
                    if (t == classNameUpperUnderscore) "_" else ""
            )

            val t2 = CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_CAMEL, f.simpleName.substring(3)) +
                    (if (b) "?.id" else "")

            val data =
                    if (b)
                        "insertedItemsIds[Pair(\"${CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, f.simpleName.substring(3))}\", data.$t2!!)] ?: data.$t2!!"
                    else
                        "data.$t2!!"
            fieldListData.add(data)
//            fieldListData.add("data.$t2!!")

            fieldListAndData.add(Pair("$classNameUpperUnderscore." + t +
                    if (t == classNameUpperUnderscore) "_" else "", data))
        }

        val fetchDataCode = CodeBlock.builder().add(
                "" +
                        "return Db.main<Result<Record>> {" +
                        "return@main it" +
                        ".select(listOf(")

        fetchDataCode.add("$classNameUpperUnderscore.ID, " + fieldList.joinToString(separator = ", "))

        fetchDataCode.add("))" +
                ".from($classNameUpperUnderscore)" +
                ".where($classNameUpperUnderscore.USER_ID.eq(user.userId))" +
                ".fetch()" +
                "}")

        val insertItemToDbCode = CodeBlock.builder().add("" +
                "return Db.main<$fileName> {" +
                "val res = it.insertInto($classNameUpperUnderscore," +
                "$classNameUpperUnderscore.USER_ID, " +
                fieldList.joinToString(separator = ", ") + ")" +
                ".values(" +
                "user.userId," +
                fieldListData.joinToString(separator = ", ") + ")" +
                ".returning(" +
                "$classNameUpperUnderscore.ID, " +
                fieldList.joinToString(separator = ", ") + ")" +
                ".fetchOne(); " +
                "return@main SyncManager.modelMapper.map(res, $fileName::class.java)" +
                "}"
        )

        val isDbRecordNewerCode = CodeBlock.builder().add("" +
                "return Db.main<Boolean> {" +
                "val res = it.select($classNameUpperUnderscore.UPDATED_AT)" +
                ".from($classNameUpperUnderscore)" +
                ".where($classNameUpperUnderscore.ID.eq(id))" +
                ".first(); " +
                "val timeStamp = res [$classNameUpperUnderscore.UPDATED_AT]; " +
                "return@main timeStamp.time > updatedAt }")

        val updateDbRecordCode = CodeBlock.builder().add("" +
                "Db.main {" +
                "it.update($classNameUpperUnderscore)" +
                fieldListAndData.joinToString(separator = "") {
                    ".set(${it.first}, ${it.second})"
                } +
                ".where($classNameUpperUnderscore.ID.eq(data.id))" +
                ".execute()" +
                "}")


        val recordClass = TypeSpec.classBuilder(fileName)
                .superclass(
                        ParameterizedTypeName.get(
                                ClassName("com.orengy.archinator.backend.model.sync.orm.base", "SyncFileRecord"),
                                ClassName("com.orengy.archinator.backend.model.sync.orm", fileName)
                        )
                )
                .addFunction(FunSpec.builder("fetchDbData")
                        .addModifiers(KModifier.OVERRIDE)
                        .addParameter(
                                "user",
                                ClassName("com.orengy.archinator.backend.model", "User")

                        )
                        .returns(ParameterizedTypeName.get(
                                ClassName("org.jooq", "Result"),
                                ClassName("org.jooq", "Record")
                        ))
                        .addCode(
                                fetchDataCode.build()
                        )
                        .build())
                .addFunction(FunSpec.builder("insertItemToDb")
                        .addModifiers(KModifier.OVERRIDE)
                        .addParameter(
                                "data",
                                ClassName(pack, fileName)
                        )
                        .addParameter(
                                "user",
                                ClassName("com.orengy.archinator.backend.model", "User")
                        )
                        .addParameter(
                                "insertedItemsIds",
                                ParameterizedTypeName.get(
                                        ClassName("kotlin.collections", "HashMap"),
                                        ParameterizedTypeName.get(
                                                Pair::class,
                                                String::class,
                                                Int::class
                                        ),
                                        Int::class.asTypeName()
                                )

                        )
                        .returns(
                                ClassName(pack, fileName)
                        )
                        .addCode(
                                insertItemToDbCode.build()
                        )
                        .build())
                .addFunction(FunSpec.builder("updateDbRecord")
                        .addModifiers(KModifier.OVERRIDE)
                        .addParameter(
                                "data",
                                ClassName(pack, fileName)
                        )
                        .addParameter(
                                "insertedItemsIds",
                                ParameterizedTypeName.get(
                                        ClassName("kotlin.collections", "HashMap"),
                                        ParameterizedTypeName.get(
                                                Pair::class,
                                                String::class,
                                                Int::class
                                        ),
                                        Int::class.asTypeName()
                                )

                        )
                        .addCode(
                                updateDbRecordCode.build()
                        )
                        .build())
                .addFunction(FunSpec.builder("isDbRecordNewer")
                        .addModifiers(KModifier.OVERRIDE)
                        .addParameter(
                                "updatedAt",
                                ClassName("kotlin", "Long")
                        )
                        .addParameter(
                                "id",
                                ClassName("kotlin", "Int")
                        )
                        .returns(
                                ClassName("kotlin", "Boolean")
                        )
                        .addCode(
                                isDbRecordNewerCode.build()
                        )
                        .build())
                .addProperty(PropertySpec.builder(
                        "listType",
                        ClassName("java.lang.reflect", "Type"),
                        KModifier.OVERRIDE)
                        .addAnnotation(Transient::class)
                        .mutable(false)
                        .initializer("object : TypeToken<List<$fileName>>() {}.type")
                        .build())

        for (f in fields) {

            val t = (f.asType() as ExecutableType).returnType.asTypeName().asNonNullable().toString()
            val typeName =
                    if (t.startsWith(pack)) {
                        ClassName(pack, t.substringAfterLast(".") + classNameAddition).asNullable()
                    } else {
                        when (t) {
                            "java.lang.Integer" -> ClassName("kotlin", "Int").asNullable()
                            "java.lang.String" -> ClassName("kotlin", "String").asNullable()
                            "java.lang.Double" -> ClassName("kotlin", "Double").asNullable()
                            else -> (f.asType() as ExecutableType).returnType.asTypeName().asNullable()
                        }


                    }

            recordClass.addProperty(PropertySpec.builder(
                    CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_CAMEL, f.simpleName.substring(3)),
                    typeName)
                    .mutable(true)
                    .initializer("null")
                    .build())
        }


        val file = FileSpec.builder(pack, fileName)
                .addStaticImport("com.orengy.archinator.backend.utils", "Db")
                .addStaticImport("com.orengy.archinator.db", "Tables.*")
                .addStaticImport("org.modelmapper", "TypeToken")
                .addStaticImport("com.orengy.archinator.backend.model.sync", "SyncManager")
                .addType(recordClass.build())
                .build()

        val kaptKotlinGeneratedDir = processingEnv.options[KAPT_KOTLIN_GENERATED_OPTION_NAME]
        file.writeTo(File(kaptKotlinGeneratedDir, "$fileName.kt"))
    }

    companion object {
        const val classNameAddition = "Record"
        const val KAPT_KOTLIN_GENERATED_OPTION_NAME = "kapt.kotlin.generated"
    }
}