Docker
===
##Mysql
###Create container
`docker run --name archinator-mysql --publish 3306:3306 -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=mhb_starter  -e MYSQL_USER=mhb -e MYSQL_PASSWORD=pass -d mysql:latest --character-set-server=utf8mb4 --collation-server=utf8mb4_unicode_ci`
###Start container
`docker start mhb-starter-mysql`
###Stop container
`docker stop mhb-starter-mysql`
###Remove container
`docker rm -f mhb-starter-mysql`
###Info:
host: localhost   
port: 6653   
user: root   
pass: root   
OR   
user: mhb   
pass: pass   

dbname: mhb_starter   
encoding: utf8mb4

##Create db in other mysql server:
`CREATE DATABASE mhb_starter  DEFAULT CHARACTER SET utf8mb4  DEFAULT COLLATE utf8mb4_unicode_ci;`   
`GRANT ALL PRIVILEGES ON *.* TO 'user'@'%' IDENTIFIED BY 'password' WITH GRANT OPTION;`    
`FLUSH PRIVILEGES;`   
##Generate api docs (node js is needed)
###Install
`npm install apidoc -g`
###Generate docs
`apidoc -i ./src -o src/main/resources/public/apidocs`