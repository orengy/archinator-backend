package functional

import io.restassured.RestAssured
import org.junit.Test

/**
 * Created by Adomas on 2017-07-17.
 */
abstract class FunctionalTest(val pingUrls: List<Pair<String, Int>>) : BaseFunctionalTest() {
    @Test
    fun basePing() {
        for (ping in pingUrls) {
            RestAssured.given()
                    .`when`()
                    .get(ping.first)
                    .then()
                    .statusCode(ping.second)
        }
    }

}