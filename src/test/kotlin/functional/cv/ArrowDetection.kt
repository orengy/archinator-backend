package functional.cv

import com.orengy.archinator.backend.constants.Config
import com.orengy.archinator.backend.utils.Db
import com.orengy.archinator.common.*
import com.orengy.archinator.common.Target
import com.orengy.archinator.common.logger.AbstractLogger
import com.orengy.archinator.db.tables.End.END
import com.orengy.archinator.db.tables.EndMedia.END_MEDIA
import com.orengy.archinator.db.tables.Round.ROUND
import com.orengy.archinator.db.tables.User
import org.junit.Test
import org.opencv.core.*
import org.opencv.imgcodecs.Imgcodecs
import java.io.File
import java.util.LinkedList
import org.opencv.features2d.DescriptorMatcher
import org.opencv.highgui.Highgui
import org.opencv.imgcodecs.Imgcodecs.CV_LOAD_IMAGE_COLOR
import org.opencv.features2d.Features2d
import org.opencv.features2d.DescriptorExtractor
import org.opencv.features2d.FeatureDetector
import org.opencv.core.MatOfPoint2f
import org.opencv.core.MatOfByte
import org.opencv.core.MatOfDMatch
import org.opencv.calib3d.Calib3d
import org.opencv.core.Mat
import org.opencv.imgproc.Imgproc


class ArrowDetection {

    inner class DesktopLogger(val location: String, val prefix: String) : AbstractLogger() {

        override fun log(tag: String, img: Mat) {
            Imgcodecs.imwrite(location + "/${prefix}_$tag.jpg", img)

        }
    }

    private fun urlToLocalFile(url: String): File {
        return File(Config.UPLOAD_DIR + url.substring("/v1/sync/file".length))
    }

    @Test
    fun test1() {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME)

        val camera = Camera(9, 6)
        camera.fromJsonString("{\"intrinsic\":[828.2247325294277,0.0,389.62051037125826,0.0,830.5137032475825,519.4797297179107,0.0,0.0,1.0],\"dist_coeffs\":[0.31912292123540964,-1.6751591326152708,0.0012529359419450874,-3.685044689066447E-4,2.8946816702611944]}")

        Db.main {
            val rounds = it.select(
                    ROUND.ID,
                    ROUND.REMOTE_FILE_URL)
                    .from(ROUND)

                    .fetch()
            for (round in rounds) {
                val referencePhoto = urlToLocalFile(round[ROUND.REMOTE_FILE_URL])
                println(referencePhoto.absolutePath)
                val roundId = round[END.ID]

                val file2 = Imgcodecs.imread(referencePhoto.absolutePath);
                val file = Mat()
                camera.undistort(file2, file)
//                MatHelper.resize(file)

                val referenceTarget = ReferenceTarget(file, DesktopLogger("../out", "round$roundId"))


                val ends = it.select(
                        END.ID)
                        .from(END)
                        .where(END.ROUND_ID.eq(roundId))
                        .fetch()

                for (end in ends) {
                    val endMedia = it.select(
                            END_MEDIA.ID,
                            END_MEDIA.REMOTE_FILE_URL)
                            .from(END_MEDIA)
                            .where(END_MEDIA.END_ID.eq(end[END.ID]))
                            .fetch()
                    for (media in endMedia) {
                        val endMediaId = media[END_MEDIA.ID]

                        val logger = DesktopLogger("../out", "endMedia$endMediaId")

                        val arrowsImg2 = Imgcodecs.imread(urlToLocalFile(media[END_MEDIA.REMOTE_FILE_URL]).absolutePath)
                        val arrowsImg = Mat()
                        camera.undistort(arrowsImg2, arrowsImg)

                        val arrows = Arrows(arrowsImg, referenceTarget, logger)
                        val detectedArrows = arrows.findArrows()
                        val tmp = arrows.targetImg.clone()

                        detectedArrows.forEach {
                            Imgproc.circle(tmp, it.tipPoint, 2, Scalar(0.0, 255.0, 0.0), 2)
                            println(it.score)
                        }
                        logger.log("allPoints", tmp)
                        println("========")


                        //val arrowsTarget = ArrowsTarget(arrowsImg, referenceTarget, logger)


//                        val sceneImage = file
////                        val sceneImage = target.getTransformedTarget()
//                        val objectImage = Imgcodecs.imread(urlToLocalFile(media[END_MEDIA.REMOTE_FILE_URL]).absolutePath)
//
//                        val objectKeyPoints = MatOfKeyPoint()
//                        val featureDetector = FeatureDetector.create(FeatureDetector.ORB)
//                        println("Detecting key points...")
//                        featureDetector.detect(objectImage, objectKeyPoints)
//                        val keypoints = objectKeyPoints.toArray()
//                        println(keypoints)
//
//                        val objectDescriptors = MatOfKeyPoint()
//                        val descriptorExtractor = DescriptorExtractor.create(DescriptorExtractor.ORB)
//                        println("Computing descriptors...")
//                        descriptorExtractor.compute(objectImage, objectKeyPoints, objectDescriptors)
//
//                        // Create the matrix for output image.
//                        val outputImage = Mat(objectImage.rows(), objectImage.cols(), Imgcodecs.CV_LOAD_IMAGE_COLOR)
//                        val newKeypointColor = Scalar(255.0, 0.0, 0.0)
//
//                        println("Drawing key points on object image...")
//                        Features2d.drawKeypoints(objectImage, objectKeyPoints, outputImage, newKeypointColor, 0)
//
//                        // Match object image with the scene image
//                        val sceneKeyPoints = MatOfKeyPoint()
//                        val sceneDescriptors = MatOfKeyPoint()
//                        println("Detecting key points in background image...")
//                        featureDetector.detect(sceneImage, sceneKeyPoints)
//                        println("Computing descriptors in background image...")
//                        descriptorExtractor.compute(sceneImage, sceneKeyPoints, sceneDescriptors)
//
//                        val outputImageScene = Mat(sceneImage.rows(), sceneImage.cols(), Imgcodecs.CV_LOAD_IMAGE_COLOR)
//                        val newKeypointColorScene = Scalar(255.0, 0.0, 0.0)
//
//                        println("Drawing key points on object image...")
//                        Features2d.drawKeypoints(sceneImage, sceneKeyPoints, outputImageScene, newKeypointColorScene, 0)
//
//
//                        val matchoutput = Mat(sceneImage.rows() * 2, sceneImage.cols() * 2, Imgcodecs.CV_LOAD_IMAGE_COLOR)
//                        val matchestColor = Scalar(0.0, 255.0, 0.0)
//
//                        val matches = LinkedList<MatOfDMatch>()
//                        val descriptorMatcher = DescriptorMatcher.create(DescriptorMatcher.BRUTEFORCE)
//                        println("Matching object and scene images...")
//                        descriptorMatcher.knnMatch(objectDescriptors, sceneDescriptors, matches, 2)
//
//                        println("Calculating good match list...")
//                        val goodMatchesList = LinkedList<DMatch>()
//
//                        val nndrRatio = 0.9f
//
//                        for (i in matches.indices) {
//                            val matofDMatch = matches[i]
//                            val dmatcharray = matofDMatch.toArray()
//                            val m1 = dmatcharray[0]
//                            val m2 = dmatcharray[1]
//
//                            if (m1.distance <= m2.distance * nndrRatio) {
//                                goodMatchesList.addLast(m1)
//
//                            }
//                        }
//
//                        if (goodMatchesList.size >= 7) {
//
//                            val objKeypointlist = objectKeyPoints.toList()
//                            val scnKeypointlist = sceneKeyPoints.toList();
//
//                            val objectPoints = mutableListOf<Point>()
//                            val scenePoints = mutableListOf<Point>()
//
//
//                            for (i in 0 until goodMatchesList.size) {
//                                objectPoints.add(objKeypointlist[goodMatchesList[i].queryIdx].pt)
//                                scenePoints.add(scnKeypointlist[goodMatchesList[i].trainIdx].pt)
//                            }
//
//                            val objMatOfPoint2f = MatOfPoint2f()
//                            objMatOfPoint2f.fromList(objectPoints)
//                            val scnMatOfPoint2f = MatOfPoint2f()
//                            scnMatOfPoint2f.fromList(scenePoints)
//
//                            val homography = Calib3d.findHomography(objMatOfPoint2f, scnMatOfPoint2f, Calib3d.RANSAC, 3.0)
//
//                            val perspectiveImg = objectImage.clone()
//                            Imgproc.warpPerspective(objectImage, perspectiveImg, homography, objectImage.size())
//                            logger.log("wrapped", perspectiveImg)
//
////                            val detectedTargetPerspective = target.bestFittedTarget.perspective.mul(homography)
//
//                            Imgproc.warpPerspective(perspectiveImg, perspectiveImg, target.bestFittedTarget.perspective, Size(Target.TARGET_IMAGE_SIZE, Target.TARGET_IMAGE_SIZE))
//                            logger.log("wrapped_target", perspectiveImg)
//
//
//                            val closestWrapped = MatHelper.closestColors(perspectiveImg)
//                            logger.log("wrapped_closest", closestWrapped)
//                            val extractedBlack = MatHelper.extractColor(closestWrapped, Colors.COLOR_BLACK, 0.0)
//                            logger.log("blackExtracted", extractedBlack)
////
////                            val edges = perspectiveImg.clone()
////                            Imgproc.medianBlur(edges, edges, 5)
////
////                            Imgproc.cvtColor(edges, edges, Imgproc.COLOR_BGR2GRAY)
////                            Imgproc.blur(edges, edges, Size(4.0, 4.0))
////                            val t = 20.0
////                            Imgproc.Canny(edges, edges, t, t * 3)
////                            val element = Imgproc.getStructuringElement(Imgproc.MORPH_CROSS, Size(3.0, 3.0))
////                            Imgproc.dilate(edges, edges, element)
////                            logger.log("edges", edges)
//
//
//                            val goodMatches = MatOfDMatch()
//                            goodMatches.fromList(goodMatchesList)
//
//                            Features2d.drawMatches(objectImage, objectKeyPoints, sceneImage, sceneKeyPoints, goodMatches, matchoutput, matchestColor, newKeypointColor, MatOfByte(), 2)
//
////                            logger.log("matchOut", matchoutput)
//                        }
//
//
//                        println(goodMatchesList.size)
////                        logger.log("object", outputImage)
////                        logger.log("scene", sceneImage)
////                        val sceneClosest = MatHelper.closestColors(sceneImage)
////                        logger.log("scene_closest", sceneClosest)
////                        return@main
                    }
                    return@main
                }
            }

        }
    }
}