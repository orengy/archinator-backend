package functional.misc

import com.orengy.archinator.backend.utils.TimeUtil
import functional.FunctionalTest
import io.restassured.RestAssured.given
import org.junit.Test
import org.hamcrest.Matchers.*

/**
 * Created by Adomas on 2017-07-17.
 */
class TestTime : FunctionalTest(
        listOf(
                Pair("v1/misc/time", 200)
        )
) {
    @Test
    fun timeDifferenceTest() {
        val timeDifference = 100

        given()
                .`when`()
                .get("v1/misc/time")
                .then()
                .statusCode(200)
                .body("time", lessThanOrEqualTo((TimeUtil.seconds() + timeDifference).toInt()))
                .body("time", greaterThanOrEqualTo((TimeUtil.seconds() - timeDifference).toInt()))
    }
}