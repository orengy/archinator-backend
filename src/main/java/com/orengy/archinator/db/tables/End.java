/*
 * This file is generated by jOOQ.
*/
package com.orengy.archinator.db.tables;


import com.orengy.archinator.db.Archinator;
import com.orengy.archinator.db.Keys;
import com.orengy.archinator.db.tables.records.EndRecord;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Identity;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.9.2"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class End extends TableImpl<EndRecord> {

    private static final long serialVersionUID = -321384856;

    /**
     * The reference instance of <code>archinator.end</code>
     */
    public static final End END = new End();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<EndRecord> getRecordType() {
        return EndRecord.class;
    }

    /**
     * The column <code>archinator.end.id</code>.
     */
    public final TableField<EndRecord, Integer> ID = createField("id", org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * The column <code>archinator.end.created_at</code>.
     */
    public final TableField<EndRecord, Long> CREATED_AT = createField("created_at", org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

    /**
     * The column <code>archinator.end.updated_at</code>.
     */
    public final TableField<EndRecord, Timestamp> UPDATED_AT = createField("updated_at", org.jooq.impl.SQLDataType.TIMESTAMP.nullable(false).defaultValue(org.jooq.impl.DSL.inline("CURRENT_TIMESTAMP", org.jooq.impl.SQLDataType.TIMESTAMP)), this, "");

    /**
     * The column <code>archinator.end.user_id</code>.
     */
    public final TableField<EndRecord, Integer> USER_ID = createField("user_id", org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * The column <code>archinator.end.round_id</code>.
     */
    public final TableField<EndRecord, Integer> ROUND_ID = createField("round_id", org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * Create a <code>archinator.end</code> table reference
     */
    public End() {
        this("end", null);
    }

    /**
     * Create an aliased <code>archinator.end</code> table reference
     */
    public End(String alias) {
        this(alias, END);
    }

    private End(String alias, Table<EndRecord> aliased) {
        this(alias, aliased, null);
    }

    private End(String alias, Table<EndRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, "");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return Archinator.ARCHINATOR;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Identity<EndRecord, Integer> getIdentity() {
        return Keys.IDENTITY_END;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<EndRecord> getPrimaryKey() {
        return Keys.KEY_END_PRIMARY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<EndRecord>> getKeys() {
        return Arrays.<UniqueKey<EndRecord>>asList(Keys.KEY_END_PRIMARY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ForeignKey<EndRecord, ?>> getReferences() {
        return Arrays.<ForeignKey<EndRecord, ?>>asList(Keys.END_IBFK_1, Keys.END_IBFK_2);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public End as(String alias) {
        return new End(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public End rename(String name) {
        return new End(name, null);
    }
}
