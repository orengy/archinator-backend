/*
 * This file is generated by jOOQ.
*/
package com.orengy.archinator.db.tables.records;


import com.orengy.archinator.db.tables.EndMedia;

import java.sql.Timestamp;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record8;
import org.jooq.Row8;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.9.2"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class EndMediaRecord extends UpdatableRecordImpl<EndMediaRecord> implements Record8<Integer, Long, Timestamp, Integer, Integer, Integer, Integer, String> {

    private static final long serialVersionUID = 254490435;

    /**
     * Setter for <code>archinator.end_media.id</code>.
     */
    public EndMediaRecord setId(Integer value) {
        set(0, value);
        return this;
    }

    /**
     * Getter for <code>archinator.end_media.id</code>.
     */
    public Integer getId() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>archinator.end_media.created_at</code>.
     */
    public EndMediaRecord setCreatedAt(Long value) {
        set(1, value);
        return this;
    }

    /**
     * Getter for <code>archinator.end_media.created_at</code>.
     */
    public Long getCreatedAt() {
        return (Long) get(1);
    }

    /**
     * Setter for <code>archinator.end_media.updated_at</code>.
     */
    public EndMediaRecord setUpdatedAt(Timestamp value) {
        set(2, value);
        return this;
    }

    /**
     * Getter for <code>archinator.end_media.updated_at</code>.
     */
    public Timestamp getUpdatedAt() {
        return (Timestamp) get(2);
    }

    /**
     * Setter for <code>archinator.end_media.user_id</code>.
     */
    public EndMediaRecord setUserId(Integer value) {
        set(3, value);
        return this;
    }

    /**
     * Getter for <code>archinator.end_media.user_id</code>.
     */
    public Integer getUserId() {
        return (Integer) get(3);
    }

    /**
     * Setter for <code>archinator.end_media.end_id</code>.
     */
    public EndMediaRecord setEndId(Integer value) {
        set(4, value);
        return this;
    }

    /**
     * Getter for <code>archinator.end_media.end_id</code>.
     */
    public Integer getEndId() {
        return (Integer) get(4);
    }

    /**
     * Setter for <code>archinator.end_media.detected_arrows</code>.
     */
    public EndMediaRecord setDetectedArrows(Integer value) {
        set(5, value);
        return this;
    }

    /**
     * Getter for <code>archinator.end_media.detected_arrows</code>.
     */
    public Integer getDetectedArrows() {
        return (Integer) get(5);
    }

    /**
     * Setter for <code>archinator.end_media.processing_time</code>.
     */
    public EndMediaRecord setProcessingTime(Integer value) {
        set(6, value);
        return this;
    }

    /**
     * Getter for <code>archinator.end_media.processing_time</code>.
     */
    public Integer getProcessingTime() {
        return (Integer) get(6);
    }

    /**
     * Setter for <code>archinator.end_media.remote_file_url</code>.
     */
    public EndMediaRecord setRemoteFileUrl(String value) {
        set(7, value);
        return this;
    }

    /**
     * Getter for <code>archinator.end_media.remote_file_url</code>.
     */
    public String getRemoteFileUrl() {
        return (String) get(7);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Record1<Integer> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record8 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row8<Integer, Long, Timestamp, Integer, Integer, Integer, Integer, String> fieldsRow() {
        return (Row8) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row8<Integer, Long, Timestamp, Integer, Integer, Integer, Integer, String> valuesRow() {
        return (Row8) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field1() {
        return EndMedia.END_MEDIA.ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Long> field2() {
        return EndMedia.END_MEDIA.CREATED_AT;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Timestamp> field3() {
        return EndMedia.END_MEDIA.UPDATED_AT;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field4() {
        return EndMedia.END_MEDIA.USER_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field5() {
        return EndMedia.END_MEDIA.END_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field6() {
        return EndMedia.END_MEDIA.DETECTED_ARROWS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field7() {
        return EndMedia.END_MEDIA.PROCESSING_TIME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field8() {
        return EndMedia.END_MEDIA.REMOTE_FILE_URL;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value1() {
        return getId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long value2() {
        return getCreatedAt();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Timestamp value3() {
        return getUpdatedAt();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value4() {
        return getUserId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value5() {
        return getEndId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value6() {
        return getDetectedArrows();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value7() {
        return getProcessingTime();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value8() {
        return getRemoteFileUrl();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EndMediaRecord value1(Integer value) {
        setId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EndMediaRecord value2(Long value) {
        setCreatedAt(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EndMediaRecord value3(Timestamp value) {
        setUpdatedAt(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EndMediaRecord value4(Integer value) {
        setUserId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EndMediaRecord value5(Integer value) {
        setEndId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EndMediaRecord value6(Integer value) {
        setDetectedArrows(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EndMediaRecord value7(Integer value) {
        setProcessingTime(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EndMediaRecord value8(String value) {
        setRemoteFileUrl(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EndMediaRecord values(Integer value1, Long value2, Timestamp value3, Integer value4, Integer value5, Integer value6, Integer value7, String value8) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        value6(value6);
        value7(value7);
        value8(value8);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached EndMediaRecord
     */
    public EndMediaRecord() {
        super(EndMedia.END_MEDIA);
    }

    /**
     * Create a detached, initialised EndMediaRecord
     */
    public EndMediaRecord(Integer id, Long createdAt, Timestamp updatedAt, Integer userId, Integer endId, Integer detectedArrows, Integer processingTime, String remoteFileUrl) {
        super(EndMedia.END_MEDIA);

        set(0, id);
        set(1, createdAt);
        set(2, updatedAt);
        set(3, userId);
        set(4, endId);
        set(5, detectedArrows);
        set(6, processingTime);
        set(7, remoteFileUrl);
    }
}
