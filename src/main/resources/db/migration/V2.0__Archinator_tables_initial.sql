CREATE TABLE unit
(
  id   INT          NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(255) NOT NULL
);

CREATE TABLE distance
(
  id       INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  distance INT NOT NULL,
  unit_id  INT NOT NULL
);

CREATE INDEX fk_distance_unit
  ON distance (unit_id);

ALTER TABLE distance
  ADD CONSTRAINT fk_distance_unit
FOREIGN KEY (unit_id) REFERENCES unit (id)
  ON UPDATE CASCADE
  ON DELETE CASCADE;

CREATE TABLE target_face
(
  id       INT          NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name     VARCHAR(255) NOT NULL
);