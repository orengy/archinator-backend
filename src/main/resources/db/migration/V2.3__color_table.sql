CREATE TABLE color
(
  id   INT          NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(255) NOT NULL,
  hex  INT          NOT NULL,
  code VARCHAR(255) NOT NULL
);


INSERT INTO color (name, hex, code) VALUES
  ('White', CONV('FFFFFF', 16, 10), 'W'),
  ('Silver', CONV('9E9E9E', 16, 10), 'S'),
  ('Black', CONV('000000', 16, 10), 'K'),
  ('Red', CONV('F44336', 16, 10), 'R'),
  ('Pink', CONV('E91E63', 16, 10), 'P'),
  ('Indigo', CONV('9C27B0', 16, 10), 'I'),
  ('Blue', CONV('2196F3', 16, 10), 'B'),
  ('Green', CONV('4CAF50', 16, 10), 'G'),
  ('Yellow', CONV('FFEB3B', 16, 10), 'Y'),
  ('Orange', CONV('FF9800', 16, 10), 'O');


