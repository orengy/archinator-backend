CREATE TABLE round_weather
(
  id             INT       NOT NULL AUTO_INCREMENT PRIMARY KEY,
  created_at     BIGINT NOT NULL,
  updated_at     TIMESTAMP NOT NULL DEFAULT NOW() ON UPDATE now(),
  user_id        INT       NOT NULL,
  round_id       INT       NOT NULL,
  time           INT       NOT NULL,
  temperature    INT       NOT NULL,
  humidity       INT       NOT NULL,
  wind_speed     DOUBLE    NOT NULL,
  wind_direction INT       NOT NULL,

  INDEX (user_id),
  FOREIGN KEY (user_id) REFERENCES user (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE,
  INDEX (round_id),
  FOREIGN KEY (round_id) REFERENCES round (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE
);
