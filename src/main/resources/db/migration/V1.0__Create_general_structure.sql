CREATE TABLE profile
(
  user_id INT          NOT NULL    PRIMARY KEY,
  name    VARCHAR(255) NULL,
  email   VARCHAR(255) NULL
);

CREATE TABLE social_account
(
  id         INT          NOT NULL AUTO_INCREMENT PRIMARY KEY,
  user_id    INT          NOT NULL,
  provider   VARCHAR(255) NOT NULL,
  client_id  VARCHAR(255) NOT NULL,
  created_at INT          NOT NULL,
  data       TEXT         NULL,
  CONSTRAINT account_unique
  UNIQUE (provider, client_id)
);

CREATE INDEX fk_user_account
  ON social_account (user_id);

CREATE TABLE user
(
  id              INT          NOT NULL AUTO_INCREMENT PRIMARY KEY,
  uid             VARCHAR(255) NOT NULL,
  registration_ip BIGINT       NULL,
  created_at      INT          NOT NULL,
  CONSTRAINT user_unique_uid
  UNIQUE (uid)
);

ALTER TABLE profile
  ADD CONSTRAINT fk_user_profile
FOREIGN KEY (user_id) REFERENCES user (id)
  ON UPDATE CASCADE
  ON DELETE CASCADE;

ALTER TABLE social_account
  ADD CONSTRAINT fk_user_account
FOREIGN KEY (user_id) REFERENCES user (id)
  ON UPDATE CASCADE
  ON DELETE CASCADE;

