CREATE TABLE target_face_size
(
  id             INT          NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name           VARCHAR(255) NOT NULL,
  target_face_id INT          NOT NULL
);

CREATE INDEX fk_target_face_size_target_face
  ON target_face_size (target_face_id);

ALTER TABLE target_face_size
  ADD CONSTRAINT fk_target_face_size_target_face
FOREIGN KEY (target_face_id) REFERENCES target_face (id)
  ON UPDATE CASCADE
  ON DELETE CASCADE;

CREATE TABLE target_face_scores
(
  id             INT          NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name           VARCHAR(255) NOT NULL,
  target_face_id INT          NOT NULL
);

CREATE INDEX fk_target_face_scores_target_face
  ON target_face_scores (target_face_id);

ALTER TABLE target_face_scores
  ADD CONSTRAINT fk_target_face_scores_target_face
FOREIGN KEY (target_face_id) REFERENCES target_face (id)
  ON UPDATE CASCADE
  ON DELETE CASCADE;

INSERT INTO target_face (id, name) VALUES (1, 'WA Full');

INSERT INTO target_face_size (target_face_id, name) VALUES
  (1, '40cm'),
  (1, '60cm'),
  (1, '80cm'),
  (1, '92cm'),
  (1, '122cm');

INSERT INTO target_face_scores (target_face_id, name) VALUES
  (1, 'Recurve (X-1)'),
  (1, 'Recurve (10-1)'),
  (1, 'Compound'),
  (1, '9, 7, 5, 3, 1');