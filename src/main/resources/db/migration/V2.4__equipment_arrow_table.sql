CREATE TABLE equipment_arrow
(
  id                       INT          NOT NULL AUTO_INCREMENT PRIMARY KEY,
  created_at               BIGINT       NOT NULL,
  updated_at               TIMESTAMP    NOT NULL DEFAULT NOW() ON UPDATE now(),
  user_id                  INT          NOT NULL,
  name                     VARCHAR(255) NOT NULL,
  diameter_in_mm           DOUBLE       NOT NULL,
  fletching_one_color_id   INT          NOT NULL,
  fletching_two_color_id   INT          NOT NULL,
  fletching_three_color_id INT          NOT NULL,
  nock_color_id            INT          NOT NULL,
  shaft_color_id           INT          NOT NULL,

  INDEX (user_id),
  FOREIGN KEY (user_id) REFERENCES user (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE,
  INDEX (fletching_one_color_id),
  FOREIGN KEY (fletching_one_color_id) REFERENCES color (id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
  INDEX (fletching_two_color_id),
  FOREIGN KEY (fletching_two_color_id) REFERENCES color (id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
  INDEX (fletching_three_color_id),
  FOREIGN KEY (fletching_three_color_id) REFERENCES color (id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
  INDEX (nock_color_id),
  FOREIGN KEY (nock_color_id) REFERENCES color (id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
  INDEX (shaft_color_id),
  FOREIGN KEY (shaft_color_id) REFERENCES color (id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);


