CREATE TABLE camera
(
  id              INT          NOT NULL AUTO_INCREMENT PRIMARY KEY,
  created_at      BIGINT    NOT NULL,
  updated_at      TIMESTAMP    NOT NULL DEFAULT NOW() ON UPDATE now(),
  user_id         INT          NOT NULL,
  device_code     VARCHAR(255) NOT NULL,
  calibrated_json TEXT         NOT NULL,

  INDEX (user_id),
  FOREIGN KEY (user_id) REFERENCES user (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE
);

CREATE TABLE camera_calibration
(
  id              INT          NOT NULL AUTO_INCREMENT PRIMARY KEY,
  created_at      BIGINT    NOT NULL,
  updated_at      TIMESTAMP    NOT NULL DEFAULT NOW() ON UPDATE now(),
  user_id         INT          NOT NULL,
  camera_id       INT          NOT NULL,
  remote_file_url VARCHAR(255) NOT NULL,

  INDEX (user_id),
  FOREIGN KEY (user_id) REFERENCES user (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE ,
  INDEX (camera_id),
  FOREIGN KEY (camera_id) REFERENCES camera (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE

);

CREATE TABLE session
(
  id                 INT          NOT NULL AUTO_INCREMENT PRIMARY KEY,
  created_at         BIGINT    NOT NULL,
  updated_at         TIMESTAMP    NOT NULL DEFAULT NOW() ON UPDATE now(),
  user_id            INT          NOT NULL,
  title              VARCHAR(255) NOT NULL,
  equipment_arrow_id INT          NOT NULL,


  INDEX (user_id),
  FOREIGN KEY (user_id) REFERENCES user (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE ,
  INDEX (equipment_arrow_id),
  FOREIGN KEY (equipment_arrow_id) REFERENCES equipment_arrow (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE
);

CREATE TABLE round
(
  id                    INT          NOT NULL AUTO_INCREMENT PRIMARY KEY,
  created_at            BIGINT    NOT NULL,
  updated_at            TIMESTAMP    NOT NULL DEFAULT NOW() ON UPDATE now(),
  user_id               INT          NOT NULL,
  remote_file_url       VARCHAR(255) NOT NULL,
  session_id            INT          NOT NULL,
  arrows_per_end        INT          NOT NULL,
  ends_count            INT          NOT NULL,
  target_face_id        INT          NOT NULL,
  target_face_size_id   INT          NOT NULL,
  target_face_scores_id INT          NOT NULL,
  distance_id           INT          NOT NULL,


  INDEX (user_id),
  FOREIGN KEY (user_id) REFERENCES user (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE,
  INDEX (session_id),
  FOREIGN KEY (session_id) REFERENCES session (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE,
  INDEX (target_face_id),
  FOREIGN KEY (target_face_id) REFERENCES target_face (id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
  INDEX (target_face_size_id),
  FOREIGN KEY (target_face_size_id) REFERENCES target_face_size (id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
  INDEX (target_face_scores_id),
  FOREIGN KEY (target_face_scores_id) REFERENCES target_face_scores (id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
  INDEX (distance_id),
  FOREIGN KEY (distance_id) REFERENCES distance (id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);


CREATE TABLE end
(
  id         INT       NOT NULL AUTO_INCREMENT PRIMARY KEY,
  created_at BIGINT NOT NULL,
  updated_at TIMESTAMP NOT NULL DEFAULT NOW() ON UPDATE now(),
  user_id    INT       NOT NULL,
  round_id   INT       NOT NULL,

  INDEX (user_id),
  FOREIGN KEY (user_id) REFERENCES user (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE,
  INDEX (round_id),
  FOREIGN KEY (round_id) REFERENCES round (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE
);

CREATE TABLE end_arrow
(
  id         INT       NOT NULL AUTO_INCREMENT PRIMARY KEY,
  created_at BIGINT NOT NULL,
  updated_at TIMESTAMP NOT NULL DEFAULT NOW() ON UPDATE now(),
  user_id    INT       NOT NULL,
  end_id     INT       NOT NULL,
  pos_x      DOUBLE    NOT NULL,
  pos_y      DOUBLE    NOT NULL,
  score      INT       NOT NULL,

  INDEX (user_id),
  FOREIGN KEY (user_id) REFERENCES user (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE,
  INDEX (end_id),
  FOREIGN KEY (end_id) REFERENCES end (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE
);

CREATE TABLE end_media
(
  id              INT          NOT NULL AUTO_INCREMENT PRIMARY KEY,
  created_at      BIGINT    NOT NULL,
  updated_at      TIMESTAMP    NOT NULL DEFAULT NOW() ON UPDATE now(),
  user_id         INT          NOT NULL,
  end_id          INT          NOT NULL,
  detected_arrows INT          NOT NULL,
  processing_time INT          NOT NULL,
  remote_file_url VARCHAR(255) NOT NULL,


  INDEX (user_id),
  FOREIGN KEY (user_id) REFERENCES user (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE,

  INDEX (end_id),
  FOREIGN KEY (end_id) REFERENCES end (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE

);
CREATE TABLE end_media_arrow
(
  id           INT       NOT NULL AUTO_INCREMENT PRIMARY KEY,
  created_at   BIGINT NOT NULL,
  updated_at   TIMESTAMP NOT NULL DEFAULT NOW() ON UPDATE now(),
  user_id      INT       NOT NULL,
  end_media_id INT       NOT NULL,
  pos_x        DOUBLE    NOT NULL,
  pos_y        DOUBLE    NOT NULL,
  score        INT       NOT NULL,

  INDEX (user_id),
  FOREIGN KEY (user_id) REFERENCES user (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE,

  INDEX (end_media_id),
  FOREIGN KEY (end_media_id) REFERENCES end_media (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE
);



