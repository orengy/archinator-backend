package com.orengy.archinator.backend.constants

import java.util.*


/**
 * Created by Adomas on 2017-06-28.
 */
object Config {
    private val propFileName = "config.properties"
    private val props = Properties()

    init {
        val inputStream = javaClass.classLoader.getResourceAsStream(propFileName)
        if (inputStream != null) {
            props.load(inputStream)
            inputStream.close()
        }
    }

    private fun get(key: String, default: String): String {
        return props.getProperty(key, default.toString())
    }

    private fun get(key: String, default: Int): Int {
        return props.getProperty(key, default.toString()).toInt()
    }

    private fun get(key: String, default: Boolean): Boolean {
        return props.getProperty(key, default.toString()).toBoolean()
    }

    val JWT_SECRET = get("jwt_secret", "secret")
    val JWT_ISSUER = get("jwt_issuer", "Orengy")
    val JWT_VALID_FOR = get("jwt_valid_for", 60 * 60 * 24 * 30) //in seconds

    val DB_URL = get("db_url", "jdbc:mysql://localhost:3306/archinator")
    val DB_USER = get("db_user", "user")
    val DB_PASSWORD = get("db_password", "pass")

    val PROXY_HTTP_ENABLE = get("proxy_http_enable", false)
    val PROXY_HTTP_HOST = get("proxy_http_host", "localhost")
    val PROXY_HTTP_PORT = get("proxy_http_port", 1080)


    val UPLOAD_DIR = get("upload_dir", "uploads")

}