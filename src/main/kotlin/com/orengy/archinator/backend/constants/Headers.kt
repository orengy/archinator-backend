package com.orengy.archinator.backend.constants

/**
 * Created by Adomas on 2017-06-26.
 */
object Headers {
   val ACCESS_TOKEN = "Access-Token"
}