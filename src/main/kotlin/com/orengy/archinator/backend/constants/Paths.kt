package com.orengy.archinator.backend.constants

/**
 * Created by Adomas on 2017-06-26.
 */
object Paths {
    object Auth {
        val LOGIN = "/auth/login"
    }

    object Misc {
        val TIME = "/misc/time"
        val INFO = "/misc/info"
        val EXCEPTION = "/misc/exception"
    }

    object Sync {
        val INDEX = "/sync"
        val FILE = "/sync/file"
        val FILE_GET = "/sync/file/*"
    }
}