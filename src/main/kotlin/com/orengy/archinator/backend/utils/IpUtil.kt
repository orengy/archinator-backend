package com.orengy.archinator.backend.utils

import org.apache.http.client.fluent.Request
import java.util.regex.Pattern

/**
 * Created by Adomas on 2017-06-28.
 */
object IpUtil {
    private val PATTERN = Pattern.compile(
            "^(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])$")

    private fun validate(ip: String): Boolean {
        return PATTERN.matcher(ip).matches()
    }

    fun ipToLong(ipAddress: String): Long {
        if (!validate(ipAddress))
            return 0

        val ipAddressInArray = ipAddress.split("\\.".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        var result: Long = 0
        for (i in ipAddressInArray.indices) {
            val power = 3 - i
            val ip = Integer.parseInt(ipAddressInArray[i])
            result += (ip * Math.pow(256.0, power.toDouble())).toLong()

        }
        return result
    }

    fun serverIpAddress():String{
        return Request.Get("https://api.ipify.org").execute().returnContent().asString()
    }
}