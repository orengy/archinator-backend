package com.orengy.archinator.backend.utils

import java.util.*
import java.util.concurrent.TimeUnit
import java.time.format.DateTimeFormatter
import java.text.SimpleDateFormat





/**
 * Created by Adomas on 2017-06-28.
 */
object TimeUtil {
    fun milis(): Long {
        return System.currentTimeMillis()
    }

    fun seconds(): Long {
        return TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())
    }

    fun dateString(pattern:String = "yyyyMMdd"): String {
        val date = Date()
        return SimpleDateFormat(pattern).format(date)
    }
}