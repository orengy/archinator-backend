package com.orengy.archinator.backend.utils

import com.orengy.archinator.backend.constants.Config
import org.jooq.DSLContext
import org.jooq.SQLDialect
import org.jooq.impl.DSL
import org.apache.commons.dbcp2.BasicDataSource


/**
 * Created by Adomas on 2017-05-21.
 */

object Db {

    private val mainDataSource = BasicDataSource()

    init {
        mainDataSource.url = Config.DB_URL
        mainDataSource.username = Config.DB_USER
        mainDataSource.password = Config.DB_PASSWORD
        mainDataSource.minIdle = 5
        mainDataSource.maxIdle = 10
        mainDataSource.maxOpenPreparedStatements = 100
    }

    fun main(body: (ctx: DSLContext) -> Unit) {
        DSL.using(
                mainDataSource, SQLDialect.MYSQL
        ).use { ctx ->
            body(ctx)
            ctx.close()
        }
    }

    fun <T> main(body: (ctx: DSLContext) -> T): T {
        var a: T? = null
        DSL.using(
                mainDataSource, SQLDialect.MYSQL
        ).use { ctx ->
            a = body(ctx)
        }
        return a!!
    }

}