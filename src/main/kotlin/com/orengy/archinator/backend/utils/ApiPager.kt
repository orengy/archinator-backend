package com.orengy.archinator.backend.utils

import org.apache.http.client.utils.URIBuilder

/**
 * Created by Adomas on 2017-07-03.
 */
class ApiPager(currentUrl: String) {
    @Transient private val url = URIBuilder(currentUrl)

    @Transient var totalRecords = -1
        set(value) {
            field = value
            update()
        }
    @Transient var defaultPageSize: Int = 10
        set(value) {
            field = value
            update()
        }
    @Transient var pageSize: Int = -1
        get() = if (field > 0) field else defaultPageSize
        private set
    @Transient var page: Int = 1
        private set

    init {
        url.queryParams.stream().forEach {
            if (it.name == "page") {
                page = it.value.toInt()
            }
            if (it.name == "per_page") {
                pageSize = it.value.toInt()
            }
        }

    }

    fun update() {

        pageSize = if (pageSize > 0) pageSize else defaultPageSize

        total = if (totalRecords >= 0) totalRecords else null

        if (page * pageSize < totalRecords) {
            url.setParameter("page", (page + 1).toString())
            next = url.toString()
            url.setParameter("page", (Math.ceil(totalRecords * 1.0 / pageSize).toInt()).toString())
            last = url.toString()
        } else {
            next = null
            last = null
        }

        if (page > 1) {
            url.setParameter("page", (page - 1).toString())
            prev = url.toString()

            url.setParameter("page", "1")
            first = url.toString()
        } else {
            prev = null
            first = null
        }
    }

    var next: String? = null
    var prev: String? = null
    var first: String? = null
    var last: String? = null
    var total: Int? = null

}