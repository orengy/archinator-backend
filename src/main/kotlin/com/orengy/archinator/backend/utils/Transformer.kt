package com.orengy.archinator.backend.utils

import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder

/**
 * Created by Adomas on 2017-06-27.
 */
object Transformer {
    private val gson = GsonBuilder()
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .create()

    val toJson: (Any) -> String = { gson.toJson(it) }
}