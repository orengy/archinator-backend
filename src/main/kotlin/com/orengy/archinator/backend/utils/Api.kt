package com.orengy.archinator.backend.utils

import com.orengy.archinator.backend.model.User
import com.orengy.archinator.backend.model.response.BaseResponse
import spark.Request
import spark.Spark

/**
 * Created by Adomas on 2017-06-27.
 */
class Api {
    lateinit var currentUser: User
    var clientIp: String = "0.0.0.0"
    var clientIpLong: Long = 0
    lateinit var pager: ApiPager
    lateinit var hostAddress: String

    fun initCurrentUser(accessToken: String?) {
        currentUser = User.fromAccessToken(accessToken ?: "")
    }

    fun ok(data: BaseResponse): BaseResponse {
        data.addPager(pager)
        return data
    }

    companion object {
        fun current(req: Request): Api {
            return req.session().attribute("api")
        }


        fun badRequest(message: String) {
            error(message, 400)
        }

        fun notFound(message: String) {
            error(message, 404)
        }

        fun error(message: String, code: Int = 400) {
            Spark.halt(code, jsonMessage(message))
        }

        private fun jsonMessage(message: String): String {
            return "{\"message\":\"$message\"}"
        }
    }
}