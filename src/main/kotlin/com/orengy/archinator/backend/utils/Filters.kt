package com.orengy.archinator.backend.utils

import spark.Request
import spark.Response


/**
 * Created by Adomas on 2017-06-27.
 */
object Filters {

    // If a user manually manipulates paths and forgets to add
    // a trailing slash, redirect the user to the correct path
    var addTrailingSlashes = { request: Request, response: Response ->
        if (!request.pathInfo().endsWith("/")) {
            response.redirect(request.pathInfo() + "/")
        }
    }

    // Enable GZIP for all responses
    var addGzipHeader = { request: Request, response: Response ->
        response.header("Content-Encoding", "gzip")
    }

    // Enable GZIP for all responses
    var addJsonResponseType = { request: Request, response: Response ->
        response.type("application/json")
    }
}