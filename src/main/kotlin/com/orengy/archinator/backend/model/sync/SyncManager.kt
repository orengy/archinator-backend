package com.orengy.archinator.backend.model.sync

import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.orengy.archinator.backend.model.User
import com.orengy.archinator.backend.model.sync.orm.*
import org.modelmapper.ModelMapper
import org.modelmapper.convention.NameTokenizers
import org.modelmapper.jooq.RecordValueReader

/**
 * Created by Adomas on 2017-10-31.
 */
class SyncManager(val syncGroups: Map<String?, List<SyncBaseModel>>, val user: User) {

    val insertedItemsIds = hashMapOf<Pair<String, Int>, Int>()

    fun serverState(): List<SyncResponseModel<*>> {
        val output = mutableListOf<SyncResponseModel<*>>()

        for (i in Sync.syncTypes) {
            output.addAll(Sync.syncTypes[i.key]!!.sync(
                    i.key,
                    syncGroups[i.key],
                    user,
                    insertedItemsIds
            ))
        }
        return output
    }

    companion object {
        val gson = GsonBuilder()
                .registerTypeAdapter(SyncBaseModel::class.java, SyncModelDeserializer())
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create() ?: Gson()
        val modelMapper = ModelMapper()

        init {
            modelMapper.configuration.sourceNameTokenizer = NameTokenizers.UNDERSCORE;
            modelMapper.configuration.destinationNameTokenizer = NameTokenizers.CAMEL_CASE;
            modelMapper.configuration.addValueReader(RecordValueReader())
        }
    }

}