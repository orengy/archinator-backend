package com.orengy.archinator.backend.model.sync.orm

import com.orengy.archinator.backend.Sync

/**
 * Created by Adomas on 2017-10-31.
 */
@Sync
abstract class EquipmentArrow {
    abstract var name: String
    abstract var diameterInMm: Double
    abstract var fletchingOneColor: Color
    abstract var fletchingTwoColor: Color
    abstract var fletchingThreeColor: Color
    abstract var nockColor: Color
    abstract var shaftColor: Color
}