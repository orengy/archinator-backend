package com.orengy.archinator.backend.model.response

import com.orengy.archinator.backend.utils.ApiPager

/**
 * Created by Adomas on 2017-06-27.
 */
abstract class BaseResponse {
    var _pager: ApiPager? = null

    fun addPager(pager: ApiPager) {
        _pager = if (pager.totalRecords >= 0) pager else null
    }
}