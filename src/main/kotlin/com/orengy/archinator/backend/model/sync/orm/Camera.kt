package com.orengy.archinator.backend.model.sync.orm

import com.orengy.archinator.backend.Sync

/**
 * Created by Adomas on 2017-10-31.
 */
@Sync
abstract class Camera {
    abstract var deviceCode: String
    abstract var calibratedJson: String
}