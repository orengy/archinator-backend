package com.orengy.archinator.backend.model.response.auth

import com.orengy.archinator.backend.model.User
import com.orengy.archinator.backend.model.response.BaseResponse

/**
 * Created by Adomas on 2017-06-27.
 */
class LoginResponse(
        user: User
) : BaseResponse() {
    val accessToken = user.accessToken
    val uid = user.uid
    val email = user.email
    val name = user.name
}