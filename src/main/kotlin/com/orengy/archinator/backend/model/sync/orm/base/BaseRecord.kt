package com.orengy.archinator.backend.model.sync.orm.base

import com.orengy.archinator.backend.model.User
import com.orengy.archinator.backend.model.sync.SyncBaseModel
import com.orengy.archinator.backend.model.sync.SyncResponseModel
import org.jooq.Record
import org.jooq.Result
import java.lang.reflect.Type

/**
 * Created by Adomas on 2017-10-31.
 */
abstract class BaseRecord<T : BaseRecord<T>> {

    var id: Int? = null

    abstract val listType: Type

    abstract fun fetchDbData(user: User): Result<Record>

    abstract fun sync(type: String, clientState: List<SyncBaseModel>?, user: User, insertedItemsIds: HashMap<Pair<String, Int>, Int>): List<SyncResponseModel<T>>
}