package com.orengy.archinator.backend.model.response

/**
 * Created by Adomas on 2017-06-28.
 */
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class GoogleTokenInfoResponse {
    @SerializedName("azp")
    var source: String? = null

    @SerializedName("sub")
    var userId: String? = null

    @SerializedName("email")
    var email: String? = null

    @SerializedName("name")
    var name: String? = null
    @SerializedName("picture")
    var picture: String? = null
    @SerializedName("locale")
    var locale: String? = null
}