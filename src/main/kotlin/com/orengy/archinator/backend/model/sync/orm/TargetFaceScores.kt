package com.orengy.archinator.backend.model.sync.orm

import com.orengy.archinator.backend.SyncDown

@SyncDown
abstract class TargetFaceScores {
    abstract var name: String

    abstract var targetFace: TargetFace
}