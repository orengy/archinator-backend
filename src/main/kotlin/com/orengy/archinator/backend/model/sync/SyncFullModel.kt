package com.orengy.archinator.backend.model.sync

import com.orengy.archinator.backend.model.sync.orm.base.BaseRecord

/**
 * Created by Adomas on 2017-10-28.
 */
class SyncFullModel<T : BaseRecord<T>> : SyncBaseModel() {
    val data: T? = null
    val updatedAt: Long? = null
}