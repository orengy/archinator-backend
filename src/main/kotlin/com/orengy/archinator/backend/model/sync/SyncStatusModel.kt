package com.orengy.archinator.backend.model.sync

/**
 * Created by Adomas on 2017-10-28.
 */
class SyncStatusModel : SyncBaseModel() {
    val updatedAt: Long? = null
}