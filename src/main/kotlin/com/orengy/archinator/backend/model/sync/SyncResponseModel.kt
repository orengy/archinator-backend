package com.orengy.archinator.backend.model.sync

import com.orengy.archinator.backend.model.sync.orm.base.BaseRecord

/**
 * Created by Adomas on 2017-10-31.
 */
data class SyncResponseModel<T : BaseRecord<T>>(
        val type: String,
        val action: String,
        val id: Int?,
        var data: T? = null
) {
    init {
        data?.id = id
    }
}