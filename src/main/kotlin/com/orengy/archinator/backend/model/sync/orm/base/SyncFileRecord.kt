package com.orengy.archinator.backend.model.sync.orm.base

import com.orengy.archinator.backend.model.User
import com.orengy.archinator.backend.model.sync.*
import com.orengy.archinator.backend.model.sync.orm.Sync

/**
 * Created by Adomas on 2017-10-31.
 */
abstract class SyncFileRecord<T : SyncFileRecord<T>> : SyncRecord<T>() {

    var remoteFileUrl: String? = null


}