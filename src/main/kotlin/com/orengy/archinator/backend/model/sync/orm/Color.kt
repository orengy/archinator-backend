package com.orengy.archinator.backend.model.sync.orm

import com.orengy.archinator.backend.SyncDown
import kotlin.String
import kotlin.Int

@SyncDown
abstract class Color {

    abstract var name: String
    abstract var hex: Int
    abstract var code: String
}