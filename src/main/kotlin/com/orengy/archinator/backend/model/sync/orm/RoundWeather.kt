package com.orengy.archinator.backend.model.sync.orm

import com.orengy.archinator.backend.Sync
import com.orengy.archinator.backend.SyncFile

/**
 * Created by Adomas on 2017-10-31.
 */
@Sync
abstract class RoundWeather {
    abstract var round: Round

    abstract var time: Int
    abstract var temperature: Int
    abstract var humidity: Int
    abstract var windSpeed: Double
    abstract var windDirection: Int
}