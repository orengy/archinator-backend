package com.orengy.archinator.backend.model.sync.orm

import com.orengy.archinator.backend.SyncDown

@SyncDown
abstract class Distance {
    abstract var distance: Int
    abstract var unit: Unit
}