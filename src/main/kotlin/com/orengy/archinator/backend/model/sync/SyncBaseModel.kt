package com.orengy.archinator.backend.model.sync


/**
 * Created by Adomas on 2017-10-28.
 */
abstract class SyncBaseModel {
    val type: String? = null
    val id: Int? = null
//    val serverId: Int? = null
}