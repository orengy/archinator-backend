package com.orengy.archinator.backend.model.sync.orm

import com.orengy.archinator.backend.Sync
import com.orengy.archinator.backend.SyncFile

/**
 * Created by Adomas on 2017-10-31.
 */
@SyncFile
abstract class Round {
    abstract var session: Session

    abstract var arrowsPerEnd: Int
    abstract var endsCount: Int

    abstract var targetFace: TargetFace
    abstract var targetFaceSize: TargetFaceSize
    abstract var targetFaceScores: TargetFaceScores


    abstract var distance: Distance
}