package com.orengy.archinator.backend.model

import com.google.gson.Gson
import com.orengy.archinator.backend.model.response.GoogleTokenInfoResponse
import org.apache.http.client.fluent.Request
import com.orengy.archinator.backend.utils.Db
import com.orengy.archinator.db.Tables.*
import com.orengy.archinator.backend.utils.TimeUtil
import org.apache.commons.codec.digest.DigestUtils
import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import com.orengy.archinator.backend.constants.Config
import java.util.*
import com.auth0.jwt.exceptions.JWTVerificationException
import org.apache.http.HttpHost


/**
 * Created by Adomas on 2017-06-26.
 */
class User private constructor(val uid: String, accessTokenParam: String? = null) {
    val accessToken: String = accessTokenParam ?: generateAccessToken(uid)

    val userId = getUserIdByUid(uid)
    val invalid = userId <= 0
    val isGuest = invalid
    private var pName: String? = null
    private var pEmail: String? = null


    val name: String by lazy {
        if (pName == null)
            fetchProfileDetails()
        pName ?: "Name cannot be retrieved"
    }
    val email: String by lazy {
        if (pEmail == null)
            fetchProfileDetails()
        pEmail ?: "Name cannot be retrieved"
    }

    private fun fetchProfileDetails() {
        if (invalid)
            return

        Db.main {
            val profile = it.select(PROFILE.NAME, PROFILE.EMAIL)
                    .from(PROFILE)
                    .where(PROFILE.USER_ID.eq(userId))
                    .fetchOne()
            pName = profile.get(PROFILE.NAME)
            pEmail = profile.get(PROFILE.EMAIL)
        }
    }

    companion object {
        fun fromUid(uid: String): User {
            return User(uid)
        }

        fun fromAccessToken(accessToken: String): User {
            return User(getUidFromAccessToken(accessToken), accessToken)
        }

        private fun invalidUser(): User {
            return User("User is invalid")
        }

        private fun getUserIdByUid(uid: String): Int {
            return Db.main<Int> {
                val user = it.select(USER.ID)
                        .from(USER)
                        .where(USER.UID.eq(uid))
                        .fetchOne() ?: return@main 0
                return@main user.get(USER.ID)
            }

        }

        private fun generateAccessToken(uid: String): String {
            val algorithm = Algorithm.HMAC256(Config.JWT_SECRET)
            val token = JWT.create()
                    .withExpiresAt(Date(TimeUtil.milis() + Config.JWT_VALID_FOR.toLong() * 1000))
                    .withIssuer(Config.JWT_ISSUER)
                    .withClaim("uid", uid)
                    .sign(algorithm)
            return token
        }

        private fun getUidFromAccessToken(accessToken: String): String {
            val algorithm = Algorithm.HMAC256(Config.JWT_SECRET)
            val verifier = JWT.require(algorithm)
                    .withIssuer(Config.JWT_ISSUER)
                    .build()
            try {
                val jwt = verifier.verify(accessToken)
                return jwt.getClaim("uid").asString()
            } catch (ex: JWTVerificationException) {
                return "Token is invalid"
            }

        }

        private fun generateUid(provider: String, clientId: String): String {
            return DigestUtils.md5Hex("$clientId$provider${System.currentTimeMillis()}")
        }

        private fun signIn(provider: String, clientId: String, dataJson: String, name: String? = null, clientIp: Long, email: String?): User {
            return Db.main<User> {
                val user = it.select(USER.UID)
                        .from(SOCIAL_ACCOUNT)
                        .innerJoin(USER).on(USER.ID.eq(SOCIAL_ACCOUNT.USER_ID))
                        .where(SOCIAL_ACCOUNT.PROVIDER.eq(provider))
                        .and(SOCIAL_ACCOUNT.CLIENT_ID.eq(clientId))
                        .limit(1)
                        .fetchOne()

                if (user == null) {
                    val userDb = it.insertInto(USER, USER.UID, USER.REGISTRATION_IP, USER.CREATED_AT)
                            .values(generateUid(provider, clientId), clientIp, TimeUtil.seconds().toInt())
                            .returning(USER.ID, USER.UID)
                            .fetchOne()
                    val userId = userDb.get(USER.ID)
                    val userUid = userDb.get(USER.UID)
                    if (userId != null) {

                        it.insertInto(PROFILE, PROFILE.USER_ID, PROFILE.NAME, PROFILE.EMAIL)
                                .values(userId, name, email)
                                .execute()

                        it.insertInto(SOCIAL_ACCOUNT,
                                SOCIAL_ACCOUNT.USER_ID,
                                SOCIAL_ACCOUNT.PROVIDER,
                                SOCIAL_ACCOUNT.CLIENT_ID,
                                SOCIAL_ACCOUNT.CREATED_AT,
                                SOCIAL_ACCOUNT.DATA)
                                .values(userId, provider, clientId, TimeUtil.seconds().toInt(), dataJson)
                                .execute()

                        return@main User(userUid)
                    }
                    return@main invalidUser()
                }

                return@main User(user.get(USER.UID))
            }
        }

        fun authenticate(provider: String, token: String, identity: String, clientId: Long): User {
            var user: User = invalidUser()

            when (provider) {
                "google" -> {
                    val gson = Gson()
                    val req = Request.Get("https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=$token")
                    if (Config.PROXY_HTTP_ENABLE) {
                        req.viaProxy(HttpHost(Config.PROXY_HTTP_HOST, Config.PROXY_HTTP_PORT))
                    }
                    val jsonRaw = req.execute().returnContent().asString()
                    val r = gson.fromJson<GoogleTokenInfoResponse>(jsonRaw, GoogleTokenInfoResponse::class.java)
                    val userId = r.userId
                    if (userId != null) {
                        user = signIn(provider, userId, gson.toJson(r), r.name, clientId, r.email)
                    }
                }
            }
            return user
        }
    }
}