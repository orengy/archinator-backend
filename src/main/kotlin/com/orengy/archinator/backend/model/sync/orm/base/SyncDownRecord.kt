package com.orengy.archinator.backend.model.sync.orm.base

import com.orengy.archinator.backend.model.User
import com.orengy.archinator.backend.model.sync.SyncBaseModel
import com.orengy.archinator.backend.model.sync.SyncManager
import com.orengy.archinator.backend.model.sync.SyncResponseModel
import com.orengy.archinator.backend.model.sync.SyncDownStatusModel
import com.orengy.archinator.backend.model.sync.orm.Sync

/**
 * Created by Adomas on 2017-10-31.
 */
abstract class SyncDownRecord<T : SyncDownRecord<T>> : BaseRecord<T>() {
    override fun sync(type: String, clientState: List<SyncBaseModel>?, user: User, insertedItemsIds: HashMap<Pair<String, Int>, Int>): List<SyncResponseModel<T>> {
        val mapped: List<T> = SyncManager.modelMapper.map(fetchDbData(user), listType)
        val dbState = mapped.associateBy({ it.id }, { it }).toMutableMap()
        val output = mutableListOf<SyncResponseModel<T>>()
        if (clientState != null) {
            for (i in clientState) {
                if (!dbState.contains(i.id!!)) {
                    if (i is SyncDownStatusModel) {
                        output.add(SyncResponseModel(type, Sync.ACTION_DELETE, i.id))
                    }

                } else {
                    if (i is SyncDownStatusModel) {
                        output.add(SyncResponseModel(type, Sync.ACTION_UPDATE, i.id, dbState[i.id]!!))
                        dbState.remove(i.id)
                    }
                }
            }
        }

        for (i in dbState) {
            output.add(SyncResponseModel(type, Sync.ACTION_INSERT, i.key, i.value))
        }

        return output
    }
}