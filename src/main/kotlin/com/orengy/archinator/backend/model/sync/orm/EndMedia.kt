package com.orengy.archinator.backend.model.sync.orm

import com.orengy.archinator.backend.Sync
import com.orengy.archinator.backend.SyncFile

/**
 * Created by Adomas on 2017-10-31.
 */
@SyncFile
abstract class EndMedia {
    abstract var end: End

    abstract var detectedArrows: Int

    abstract var processingTime: Int
}