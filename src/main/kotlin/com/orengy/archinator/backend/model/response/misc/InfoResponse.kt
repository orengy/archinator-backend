package com.orengy.archinator.backend.model.response.misc

import com.orengy.archinator.backend.model.response.BaseResponse

/**
 * Created by Adomas on 2017-06-29.
 */
class InfoResponse(val time: Long, val hostPath: String?, val clientIp: String, val serverIp: String): BaseResponse()