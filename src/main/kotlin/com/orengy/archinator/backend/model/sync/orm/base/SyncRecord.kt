package com.orengy.archinator.backend.model.sync.orm.base

import com.orengy.archinator.backend.model.User
import com.orengy.archinator.backend.model.sync.*
import com.orengy.archinator.backend.model.sync.orm.Sync

/**
 * Created by Adomas on 2017-10-31.
 */
abstract class SyncRecord<T : SyncRecord<T>> : BaseRecord<T>() {

    var updatedAt: Long? = null
    var createdAt: Long? = null

    abstract fun insertItemToDb(data: T, user: User, insertedItemsIds: HashMap<Pair<String, Int>, Int>): T
    abstract fun isDbRecordNewer(updatedAt: Long, id: Int): Boolean
    abstract fun updateDbRecord(data: T, insertedItemsIds: HashMap<Pair<String, Int>, Int>)

    override fun sync(type: String, clientState: List<SyncBaseModel>?, user: User, insertedItemsIds: HashMap<Pair<String, Int>, Int>): List<SyncResponseModel<T>> {
        val mapped: List<T> = SyncManager.modelMapper.map(fetchDbData(user), listType)
        val dbState = mapped.associateBy({ it.id }, { it }).toMutableMap()
        val output = mutableListOf<SyncResponseModel<T>>()
        if (clientState != null) {
            for (i in clientState) {
                if (!dbState.contains(i.id!!)) {
                    if (i is SyncFullModel<*>) {
                        val i = i as SyncFullModel<T>
                        if (i.id!! < 0) {
                            val res = insertItemToDb(i.data!!, user, insertedItemsIds)
                            output.add(SyncResponseModel(type, Sync.ACTION_INSERT, res.id!!, res))
                            insertedItemsIds[Pair(type, i.id)] = res.id!!
                        }
                        output.add(SyncResponseModel(type, Sync.ACTION_DELETE, i.id))
                    }
                    if (i is SyncStatusModel) {
                        output.add(SyncResponseModel(type, Sync.ACTION_DELETE, i.id))
                    }
                } else {
                    if (i is SyncFullModel<*>) {
                        val i = i as SyncFullModel<T>
                        if (!isDbRecordNewer(i.updatedAt ?: 0, i.id!!)) {
                            updateDbRecord(i.data!!, insertedItemsIds)
                        } else {
                            output.add(SyncResponseModel(type, Sync.ACTION_UPDATE, i.id, dbState[i.id]!!))
                        }
                        dbState.remove(i.id)
                    }
                    if (i is SyncStatusModel) {
                        if (isDbRecordNewer(i.updatedAt ?: 0, i.id)) {
                            output.add(SyncResponseModel(type, Sync.ACTION_UPDATE, i.id, dbState[i.id]!!))
                        }
                        dbState.remove(i.id)
                    }
                }
            }
        }

        for (i in dbState) {
            output.add(SyncResponseModel(type, Sync.ACTION_INSERT, i.key, i.value))
        }

        return output
    }
}