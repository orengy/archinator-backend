package com.orengy.archinator.backend.model.sync.orm

import com.orengy.archinator.backend.SyncDown
import com.orengy.archinator.backend.model.User
import com.orengy.archinator.backend.model.sync.orm.base.BaseRecord
import com.orengy.archinator.backend.model.sync.orm.base.SyncDownRecord
import com.orengy.archinator.backend.utils.Db
import com.orengy.archinator.db.Tables.*
import org.jooq.Record
import org.jooq.Result
import org.modelmapper.TypeToken

/**
 * Created by Adomas on 2017-10-31.
 */
@SyncDown
abstract class TargetFaceSize {
    abstract var name: String
    abstract var targetFace: TargetFace
}