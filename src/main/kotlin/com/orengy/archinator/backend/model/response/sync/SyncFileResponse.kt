package com.orengy.archinator.backend.model.response.sync

import com.orengy.archinator.backend.model.response.BaseResponse
import com.orengy.archinator.backend.model.sync.SyncResponseModel

/**
 * Created by Adomas on 2017-10-31.
 */
class SyncFileResponse(val remoteFileUrl: String) : BaseResponse()