package com.orengy.archinator.backend.model.sync.orm

import com.orengy.archinator.backend.SyncDown

@SyncDown
abstract class TargetFace {
    abstract var name: String
}