package com.orengy.archinator.backend.model.sync.orm

import com.orengy.archinator.backend.Sync
import com.orengy.archinator.backend.SyncFile

/**
 * Created by Adomas on 2017-10-31.
 */
@Sync
abstract class EndMediaArrow {
    abstract var endMedia: EndMedia

    abstract var posX: Double

    abstract var posY: Double

    abstract var score: Int
}