package com.orengy.archinator.backend

import com.orengy.archinator.backend.constants.Config
import com.orengy.archinator.backend.model.User
import com.orengy.archinator.backend.utils.Db
import com.orengy.archinator.db.Tables
import com.orengy.archinator.db.tables.End.END
import com.orengy.archinator.db.tables.EndMedia.END_MEDIA
import com.orengy.archinator.db.tables.Round.ROUND
import com.orengy.archinator.db.tables.Session.SESSION
import com.orengy.archinator.db.tables.User.USER
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths

fun main(args: Array<String>) {
    val sessionNumber = 2
    val referencePhotoLocation = "C:\\Users\\Adomas\\Desktop\\reference$sessionNumber.jpg"
    val endsLocation = "C:\\Users\\Adomas\\Desktop\\${sessionNumber}r"

    Db.main {
        val user = it.select(USER.UID)
                .from(USER)
                .where(USER.ID.eq(1))
                .limit(1)
                .fetchOne()
        val userUid = user[USER.UID]

        val newSession = it.insertInto(
                SESSION,
                SESSION.CREATED_AT,
                SESSION.USER_ID,
                SESSION.TITLE,
                SESSION.EQUIPMENT_ARROW_ID)
                .values(
                        System.currentTimeMillis(),
                        1,
                        "Imported $sessionNumber",
                        1
                )
                .returning(SESSION.ID)
                .fetchOne()
        val sessionId = newSession[SESSION.ID]

        val destinationDir = File(Config.UPLOAD_DIR + "/$userUid/import$sessionNumber")
        destinationDir.mkdirs()
        Files.copy(
                File(referencePhotoLocation).toPath(),
                File(Config.UPLOAD_DIR + "/$userUid/import$sessionNumber/reference$sessionNumber.jpg").toPath()
        )

        val newRound = it.insertInto(
                ROUND,
                ROUND.CREATED_AT,
                ROUND.USER_ID,
                ROUND.REMOTE_FILE_URL,
                ROUND.SESSION_ID,
                ROUND.ARROWS_PER_END,
                ROUND.ENDS_COUNT,
                ROUND.TARGET_FACE_ID,
                ROUND.TARGET_FACE_SCORES_ID,
                ROUND.TARGET_FACE_SIZE_ID,
                ROUND.DISTANCE_ID)
                .values(
                        System.currentTimeMillis(),
                        1,
                        "/v1/sync/file/$userUid/import$sessionNumber/reference$sessionNumber.jpg",
                        sessionId,
                        3,
                        20,
                        1,
                        2,
                        2,
                        16
                )
                .returning(ROUND.ID)
                .fetchOne()
        val roundId = newRound[ROUND.ID]

        val folder = File(endsLocation)
        val listOfFiles = folder.listFiles()
        listOfFiles.sortBy { it.name }

        var lastEndId = 0

        for ((i, file) in listOfFiles.withIndex()) {
            if (i % 3 == 0) {
                val end = it.insertInto(
                        END,
                        END.CREATED_AT,
                        END.USER_ID,
                        END.ROUND_ID
                )
                        .values(
                                System.currentTimeMillis(),
                                1,
                                roundId
                        )
                        .returning(END.ID)
                        .fetchOne()
                lastEndId = end[END.ID]
            }

            Files.copy(file.toPath(), File(Config.UPLOAD_DIR + "/$userUid/import$sessionNumber/end$i.jpg").toPath())

            it.insertInto(
                    END_MEDIA,
                    END_MEDIA.CREATED_AT,
                    END_MEDIA.USER_ID,
                    END_MEDIA.END_ID,
                    END_MEDIA.DETECTED_ARROWS,
                    END_MEDIA.PROCESSING_TIME,
                    END_MEDIA.REMOTE_FILE_URL)
                    .values(
                            System.currentTimeMillis(),
                            1,
                            lastEndId,
                            0,
                            0,
                            "/v1/sync/file/$userUid/import$sessionNumber/end$i.jpg"
                    )
                    .execute()

        }

    }
}