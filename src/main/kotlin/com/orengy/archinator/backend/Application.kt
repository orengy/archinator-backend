package com.orengy.archinator.backend

import com.orengy.archinator.backend.controller.v1.MiscController
import com.orengy.archinator.backend.constants.Headers
import com.orengy.archinator.backend.constants.Paths
import com.orengy.archinator.backend.controller.v1.AuthController
import com.orengy.archinator.backend.controller.v1.SyncController
import com.orengy.archinator.backend.utils.*
import spark.Request
import spark.Response
import spark.servlet.SparkApplication
import spark.Spark.*
import spark.Spark.path


/**
 * Created by Adomas on 2017-05-20.
 */
fun main(args: Array<String>) {
    var port = 80
    if (!args.isEmpty()) {
        port = args[0].toIntOrNull() ?: 80
    }


    val app = Application(port);
    app.init();
}

class Application(val mPort: Int) : SparkApplication {

    fun setup(port: Int) {

    }

    override fun init() {
        port(mPort);

        staticFiles.location("/public");

        notFound { req, res ->
            res.type("application/json")
            "{\"message\":\"Resource is not found\"}"
        }
        internalServerError { req, res ->
            res.type("application/json")
            "{\"message\":\"Server experienced critical error. Our developers are working hard to resolve it.\"}"
        }

        before("/v1/*", Filters.addJsonResponseType)
        before("/v1/*", { req: Request, res: Response ->
            req.session(true)
            val api = Api()
            val url = req.url() + (if (req.raw().queryString == null) "" else "?" + req.raw().queryString)

            val accessToken = req.headers(Headers.ACCESS_TOKEN)
            api.initCurrentUser(accessToken)

            api.hostAddress = url.substring(0, url.indexOf("/v1"))

            api.pager = ApiPager(url)

            api.clientIp = req.ip()
            api.clientIpLong = IpUtil.ipToLong(api.clientIp)

            req.session().attribute("api", api)
        })

        enableCORS()

        get("/") { req, res ->
            res.type("application/json")
            "{\"name\":\"Archinator API\",\"version\":\"v1\"}"
        }

        path("/v1") {
            post(Paths.Auth.LOGIN, AuthController.login, Transformer.toJson)

            get(Paths.Misc.TIME, MiscController.time, Transformer.toJson)
            get(Paths.Misc.INFO, MiscController.info, Transformer.toJson)
            get(Paths.Misc.EXCEPTION, MiscController.exception, Transformer.toJson)

            post(Paths.Sync.INDEX, SyncController.index, Transformer.toJson)
            post(Paths.Sync.FILE, SyncController.file, Transformer.toJson)
            get(Paths.Sync.FILE_GET, SyncController.fileGet)
        }


        after("/v1/*", Filters.addGzipHeader)

    }

    private fun enableCORS(origin: String = "*", methods: String = "GET, POST, PATCH, PUT, OPTIONS", headers: String = "") {

        options("/*") { request, response ->

            val accessControlRequestHeaders = request.headers("Access-Control-Request-Headers")
            if (accessControlRequestHeaders != null) {
                response.header("Access-Control-Allow-Headers", accessControlRequestHeaders)
            }

            val accessControlRequestMethod = request.headers("Access-Control-Request-Method")
            if (accessControlRequestMethod != null) {
                response.header("Access-Control-Allow-Methods", accessControlRequestMethod)
            }

            "OK"
        }

        before("/*", { request, response ->
            response.header("Access-Control-Allow-Origin", origin)
//            response.header("Access-Control-Request-Method", methods)
//            response.header("Access-Control-Allow-Headers", headers)
        })
    }
}