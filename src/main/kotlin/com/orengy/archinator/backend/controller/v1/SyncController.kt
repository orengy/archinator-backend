package com.orengy.archinator.backend.controller.v1

import com.google.gson.reflect.TypeToken
import com.orengy.archinator.backend.constants.Config
import com.orengy.archinator.backend.model.response.sync.SyncFileResponse
import com.orengy.archinator.backend.model.response.sync.SyncResponse
import com.orengy.archinator.backend.model.sync.SyncBaseModel
import com.orengy.archinator.backend.model.sync.SyncManager
import com.orengy.archinator.backend.utils.Api
import com.orengy.archinator.backend.utils.TimeUtil
import org.apache.commons.codec.digest.DigestUtils
import spark.Request
import spark.Response
import javax.servlet.MultipartConfigElement
import java.io.FileOutputStream
import java.io.File
import java.nio.file.Files


/**
 * Created by Adomas on 2017-06-26.
 */
object SyncController {


    /**
     * @api {post} /v1/sync Perform Sync
     * @apiGroup Sync
     * @apiVersion 1.0.0
     * @apiSampleRequest off
     * @apiParam {String} data JSON encoded string that represents state of client that needs to be sync'ed
     *
     */
    val index = { req: Request, res: Response ->
        val api = Api.current(req)

        if (api.currentUser.isGuest) {
            Api.badRequest("User is unauthenticated")
        }

        val data = req.queryParams("data")
        println(api.currentUser.accessToken)
        println(data)
        val listType = object : TypeToken<List<SyncBaseModel>>() {}.type
        val parsedData = SyncManager.gson.fromJson<List<SyncBaseModel>>(data, listType)

        val syncGroups = parsedData.filter { it?.type != null && it?.id != null }.groupBy { it.type }

        val syncManager = SyncManager(syncGroups, api.currentUser)
        val serverState = syncManager.serverState()
        api.ok(SyncResponse(serverState))
    }

    /**
     * @api {post} /v1/sync/file Perform Sync
     * @apiGroup Sync
     * @apiVersion 1.0.0
     * @apiSampleRequest off
     * @apiParam {File} data
     *
     */
    val file = { req: Request, res: Response ->
        val api = Api.current(req)

        if (api.currentUser.isGuest) {
            Api.badRequest("User is unauthenticated")
        }

        req.attribute("org.eclipse.jetty.multipartConfig", MultipartConfigElement("/temp"))
        val file = req.raw().getPart("data")
        if (file.contentType != "image/jpeg") {
            Api.badRequest("File is not JPEG Image")
        }

        val time = TimeUtil.dateString()

        val fileLocation =
                api.currentUser.uid + "/" +
                        time + "/"
        val fileName = DigestUtils.md5Hex(file.submittedFileName + System.nanoTime()) + "." +
                file.submittedFileName.substringAfterLast(".")

        val dir = File(Config.UPLOAD_DIR + "/" + fileLocation)
        dir.mkdirs()
        val targetFile = File(Config.UPLOAD_DIR + "/" + fileLocation +
                fileName)
        targetFile.createNewFile()

        file.inputStream.use({
            val buffer = ByteArray(it.available())
            it.read(buffer)

            val outStream = FileOutputStream(targetFile)
            outStream.write(buffer)
            outStream.flush()
            outStream.close()
        })

        api.ok(SyncFileResponse( "/v1/sync/file/" + fileLocation + fileName))
    }
    /**
     * @api {get} /v1/sync/file/\* Get File
     * @apiGroup Sync
     * @apiVersion 1.0.0
     * @apiSampleRequest off
     *
     */
    val fileGet = { req: Request, res: Response ->

        val file = File(Config.UPLOAD_DIR + "/" + req.splat().first())
        if(!file.exists()){
            Api.notFound("File not found")
        }

        val raw = res.raw()
        res.type("image/jpeg")

        try {
            Files.copy(file.toPath(), raw.outputStream)
            raw.outputStream.flush()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        raw
    }
}