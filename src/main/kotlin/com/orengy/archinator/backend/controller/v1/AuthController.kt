package com.orengy.archinator.backend.controller.v1

import com.orengy.archinator.backend.model.User
import com.orengy.archinator.backend.model.response.auth.LoginResponse
import com.orengy.archinator.backend.utils.Api
import spark.Request
import spark.Response


/**
 * Created by Adomas on 2017-06-26.
 */
object AuthController {


    /**
     * @api {post} /v1/auth/login Login
     * @apiGroup Auth
     * @apiVersion 1.0.0
     * @apiSampleRequest off
     * @apiParam {String="google"} provider Provider
     * @apiParam {String} token Authentication token
     * @apiParam {String} identity User identity (email|mobile number)
     *
     */
    val login = { req: Request, res: Response ->
        val api = Api.current(req)

        val provider = req.queryParams("provider")
        val token = req.queryParams("token")
        val identity = req.queryParams("identity")

        val user = User.authenticate(provider, token, identity, api.clientIpLong)
        if (user.invalid)
            Api.badRequest("Login details are invalid")

        api.ok(LoginResponse(user))
    }
}