package com.orengy.archinator.backend.controller.v1

import com.orengy.archinator.backend.model.response.misc.InfoResponse
import com.orengy.archinator.backend.model.response.misc.TimeResponse
import com.orengy.archinator.backend.utils.Api
import com.orengy.archinator.backend.utils.IpUtil
import com.orengy.archinator.backend.utils.TimeUtil
import spark.Request
import spark.Response
import java.lang.Exception


/**
 * Created by Adomas on 2017-06-26.
 */
object MiscController {


    /**
     * @api {get} /v1/misc/time Time
     * @apiGroup Misc
     * @apiVersion 1.0.0
     *
     */
    val time = { req: Request, res: Response ->
        val api = Api.current(req)

        api.ok(TimeResponse(TimeUtil.seconds()))
    }

    /**
     * @api {get} /v1/misc/info Info
     * @apiGroup Misc
     * @apiVersion 1.0.0
     *
     */
    val info = { req: Request, res: Response ->
        val api = Api.current(req)

        api.ok(InfoResponse(TimeUtil.seconds(), req.url(), req.ip(), IpUtil.serverIpAddress()))
    }

    /**
     * @api {get} /v1/misc/exception Exception
     * @apiGroup Misc
     * @apiVersion 1.0.0
     *
     * @apiParam {String} [message="Default exception message"] Exception message
     */
    val exception = { req: Request, res: Response ->
        val message = req.queryParamOrDefault("message", "Default exception message")
        throw Exception(message)
    }
}